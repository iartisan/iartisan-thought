<title>${_title}</title>
<div class="layui-row layui-col-space15">
    <div class="layui-col-md8">
    <#include "/topic/topic_top_list.ftl">
    <#include "/topic/topic_list_data.ftl">
        <div style="text-align: center">
            <div class="laypage-main">
                <a href="${context.contextPath}/topic/query/queryPageData/all/all?currPage=1" class="laypage-next">加载更多</a>
            </div>
        </div>
    </div>
<#include "/_include/right.ftl">
</div>