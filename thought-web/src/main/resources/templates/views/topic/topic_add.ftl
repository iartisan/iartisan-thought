<title>发表新贴</title>

<div class="layui-container fly-marginTop">
    <div class="fly-panel" pad20 style="padding-top: 5px;">
        <!--<div class="fly-none">没有权限</div>-->
        <div class="layui-form layui-form-pane">
            <div class="layui-tab layui-tab-brief" lay-filter="user">
                <ul class="layui-tab-title">
                    <li class="layui-this">发表新帖<!-- 编辑帖子 --></li>
                </ul>
                <div class="layui-form layui-tab-content" style="padding: 20px 0;">
                    <div class="layui-tab-item layui-show">
                        <form class="layui-form" method="post" action="${context.contextPath}/topic/management/addData">
                            <div class="layui-row layui-col-space15 layui-form-item">
                                <div class="layui-col-md3">
                                    <label class="layui-form-label">所在专栏</label>
                                    <div class="layui-input-block">
                                        <select lay-verify="required" name="categoryKey" lay-filter="column">
                                        <#if _categories ??>
                                            <#list _categories as category>
                                                <option value="${category.categoryKey!''}">${category.categoryName!''}</option>
                                            </#list>
                                        </#if>
                                        </select>
                                    </div>
                                </div>
                                <div class="layui-col-md9">
                                    <label for="L_title" class="layui-form-label">标题</label>
                                    <div class="layui-input-block">
                                        <input type="text" id="title" name="title" required lay-verify="required"
                                               autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                            </div>
                            <div class="layui-row layui-col-space15 layui-form-item layui-hide" id="fleaMarketForm" style="border: 1px dashed #e2e2e2">
                                <div class="layui-col-md4">
                                    <label class="layui-form-label layui-bg-orange">商品名称</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="productName" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-col-md4">
                                    <label class="layui-form-label layui-bg-orange" for="L_version">商品价格</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="productPrice" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-col-md4">
                                    <label class="layui-form-label layui-bg-orange" for="L_version">联系方式</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="contactPhone" autocomplete="off" class="layui-input" placeholder="可以是微信、QQ、手机号">
                                    </div>
                                </div>
                                <div class="layui-col-md6">
                                    <label class="layui-form-label layui-bg-orange" for="L_version">商品描述</label>
                                    <div class="layui-input-block">
                                        <textarea placeholder="详细描述" class="layui-textarea" name="productDesc" maxlength="200"></textarea>
                                    </div>
                                </div>
                                <div class="layui-col-md12">
                                    <label class="layui-form-label layui-bg-orange">商品图片</label>
                                    <div class="layui-input-block">
                                        <div class="layui-upload-drag upload-img">
                                            <#--<i class="layui-icon">&#xe654;</i>-->
                                            <img id="previewImg" style="width: 92px;height: 92px;"/>
                                            <input type="hidden" name="productPic" id="productPic" />
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="layui-row layui-col-space15 layui-form-item layui-hide" id="carpoolForm" style="border: 1px dashed #e2e2e2">
                                <div class="layui-col-md4">
                                    <label class="layui-form-label layui-bg-blue">上车地点</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="startLocation" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-col-md4">
                                    <label class="layui-form-label layui-bg-blue" for="L_version">目的地</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="destination" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-col-md4">
                                    <label class="layui-form-label layui-bg-blue" for="L_version">发车日期</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="startDate" id="startDate" autocomplete="off" class="layui-input" placeholder="请输入">
                                    </div>
                                </div>
                                <div class="layui-col-md4">
                                    <label class="layui-form-label layui-bg-blue" for="L_version">发车时间</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="startTime" id="startTime" autocomplete="off" class="layui-input" placeholder="请输入">
                                    </div>
                                </div>
                                <div class="layui-col-md4">
                                    <label class="layui-form-label layui-bg-blue" for="L_version">剩余座位</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="leftSeats" autocomplete="off" class="layui-input" placeholder="请输入">
                                    </div>
                                </div>
                            </div>
                            <div class="layui-form-item layui-form-text" id="contentDiv">
                                <div class="layui-input-block">
                                    <textarea id="content" name="content"
                                              placeholder="详细描述" class="layui-textarea fly-editor"
                                              style="height: 260px;"></textarea>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="L_vercode" class="layui-form-label">验证码</label>
                                <div class="layui-input-inline">
                                    <input type="text" id="L_vercode" name="vercode" required lay-verify="required"
                                           placeholder="请回答后面的问题" autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-form-mid">
                                    <span style="color: #c00;">1+1=?</span>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <button class="layui-btn" lay-filter="btnSubmit" lay-submit="">立即发布</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="${context.contextPath}/assets/js/views/topic/topic_add.js"></script>
