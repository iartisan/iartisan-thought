<div class="fly-panel">
    <div class="fly-panel-title fly-filter">
        <a>置顶</a>
        <#--<a href="#signin" class="layui-hide-sm layui-show-xs-block fly-right" id="LAY_goSignin"
           style="color: #FF5722;">去签到</a>-->
    </div>
    <ul class="fly-list">
    <#if topTopics ?? >
        <#list topTopics as topTopic>
            <li>
                <a href="${context.contextPaht}/user/query/${topTopic.customerId}" class="fly-avatar">
                    <#if (topTopic.avatar) ??>
                        <img src="${context.contextPath}/oss/rest/download/${topTopic.avatar}"
                             alt="${topTopic.custNickname}">
                    <#else>
                        <img src="/assets/images/avatar/default.png" alt="${topTopic.custNickname}">
                    </#if>
                </a>
                <h2>
                    <a class="layui-badge">${topTopic.categoryName}</a>
                    <a href="${context.contextPath}/topic/query/queryDetailData/${topTopic.id}"
                       target="_self">${topTopic.title}</a>
                </h2>
                <div class="fly-list-badge"> <@tagcode code="topic_fine" value="${topTopic.isFine}"> </@tagcode></div>
                <div class="fly-list-info">

                    <a href="user/home.html" link>
                        <cite>${topTopic.custNickname}</cite>
                    </a>
                    <span><@tagcode code="datetime" value="${topTopic.createTime?datetime}"> </@tagcode></span>
                    <span class="layui-badge fly-badge-accept layui-hide-xs"><@tagcode code="topic_end" value="${topTopic.isEnd}"> </@tagcode></span>

                    <span class="fly-list-nums"><i class="iconfont icon-pinglun1" title="回答"></i> ${topTopic.commentCount!0}</span>
                </div>
                <div class="fly-list-badge">
                </div>
            </li>
        </#list>
    </#if>
    </ul>
</div>