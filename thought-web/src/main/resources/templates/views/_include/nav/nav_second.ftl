<div class="${layui_hide_xs!''}">
    <div class="fly-panel fly-column">
        <div class="layui-container">
            <ul class="layui-clear">
                <li name='_nav' attr-data='_index'
                    class="layui-hide-xs <#if _categoryKey ?? ><#if _categoryKey='all'>layui-this</#if></#if>"
                ><a href="/">首页</a></li>
            <#if _categories ?? >
                <#list _categories as category>
                    <li name='_nav' attr-data='${category.categoryKey!''}'
                        <#if _categoryKey ??>
                            <#if (category.categoryKey)=_categoryKey>
                        class="layui-this"
                            </#if>
                        </#if>
                    >
                        <a href="${context.contextPath}/topic/query/queryPageData/${category.categoryKey!''}/all?currPage=1">${category.categoryName!''}</a>
                    </li>
                </#list>
            </#if>
                <li class="layui-hide-xs layui-hide-sm layui-show-md-inline-block"><span class="fly-mid"></span></li>

                <!-- 用户登入后显示 -->
                <li class="layui-hide-xs layui-hide-sm layui-show-md-inline-block"><a
                        href="/my/index#index">我发表的贴</a>
                </li>
                <li class="layui-hide-xs layui-hide-sm layui-show-md-inline-block"><a
                        href="/my/index#collection">我收藏的贴</a>
                </li>
            </ul>

            <div class="fly-column-right layui-hide-xs">
                <span class="fly-search"><i class="layui-icon"></i></span>
                <a href="${context.contextPath}/topic/management/addDataPage" class="layui-btn">发表新帖</a>
            </div>
            <div class="layui-hide-sm layui-show-xs-block"
                 style="margin-top: -10px; padding-bottom: 10px; text-align: center;">
                <a href="${context.contextPath}/topic/management/addDataPage" class="layui-btn">发表新帖</a>
            </div>
        </div>
    </div>
</div>