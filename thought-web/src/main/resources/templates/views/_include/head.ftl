<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="keywords" content="${_keywords!''}">
<meta name="description" content=${_description!''}>
<link rel="stylesheet" href="/assets/plugins/layui/css/layui.css">
<link rel="stylesheet" href="/assets/css/global.css">
<script type="text/javascript" src="/assets/plugins/layui/layui.js"></script>
<script type="text/javascript">
    ${_baidutongji!''}
</script>
<link rel="shortcut icon" href="/assets/images/favicon.ico">
