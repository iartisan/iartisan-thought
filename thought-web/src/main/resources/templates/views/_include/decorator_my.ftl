<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><sitemesh:write property='title'/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="keywords" content="${_keywords!''}">
    <meta name="description" content=${_description!''}>
    <link rel="stylesheet" href="/assets/plugins/layui/css/layui.css">
    <link rel="stylesheet" href="/assets/css/global.css">
    <script type="text/javascript" src="/assets/plugins/layui/layui.js"></script>
</head>
<body>
<#include "/_include/nav/nav_first.ftl"/>
<div class="layui-container fly-marginTop fly-user-main">
    <ul class="layui-nav layui-nav-tree layui-inline" lay-filter="user">
        <li class="layui-nav-item">
            <a href="${context.contextPath}/my/home">
                <i class="layui-icon">&#xe609;</i>
                我的主页
            </a>
        </li>
        <li class="layui-nav-item <#if active_code='my_index'>layui-this</#if>">
            <a href="${context.contextPath}/my/index">
                <i class="layui-icon">&#xe612;</i>
                用户中心
            </a>
        </li>
        <li class="layui-nav-item <#if active_code='my_set'>layui-this</#if>">
            <a href="${context.contextPath}/my/set">
                <i class="layui-icon">&#xe620;</i>
                基本设置
            </a>
        </li>
        <li class="layui-nav-item <#if active_code='my_message'>layui-this</#if>">
            <a href="${context.contextPath}/my/message">
                <i class="layui-icon">&#xe611;</i>
                我的消息
            </a>
        </li>
    </ul>

    <div class="site-tree-mobile layui-hide">
        <i class="layui-icon">&#xe602;</i>
    </div>
    <div class="site-mobile-shade"></div>

    <div class="site-tree-mobile layui-hide">
        <i class="layui-icon">&#xe602;</i>
    </div>
    <div class="site-mobile-shade"></div>

    <div class="fly-panel fly-panel-user" pad20>
        <sitemesh:write property='body'/>
    </div>
</div>
<#include "/_include/footer.ftl">
</body>
</html>