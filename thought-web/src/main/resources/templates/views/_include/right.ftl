<div class="layui-col-md4">

    <div class="fly-panel">
        <h3 class="fly-panel-title">温馨通道</h3>
        <ul class="fly-panel-main fly-list-static" id="_warmChannelUl">
            <script id="_warmChannel" type="text/html">
                <ul>
                    {{# layui.each(d.data, function(index, item){ }}
                    <li>
                        <a href="${context.contextPath}/topic/query/queryDetailData/{{ item.channelLocation}}"
                           target="_blank">{{ item.channelName}}</a>
                    </li>
                    {{# }); }}
                    {{# if(d.data.length === 0){ }}
                    <span class="fly-none">等待添加</span>
                    {{# } }}
                </ul>
            </script>
        </ul>
    </div>

    <dl class="fly-panel fly-list-one">
        <dt class="fly-panel-title">本周热议</dt>
        <div id="_hotTopicsDt"></div>
        <script id="_hotTopicScript" type="text/html">
            {{#  layui.each(d.data, function(index, item){ }}
            <dd>
                <a href="${context.contextPath}/topic/query/queryDetailData/{{ item.id}}">{{item.title}}</a>
                <span><i class="iconfont icon-pinglun1"></i> {{item.commentCount}}</span>
            </dd>
            {{#  }); }}
            {{#  if(d.data.length === 0){ }}
            <div class="fly-none">没有相关数据</div>
            {{#  } }}
        </script>
        </ul>
    </dl>

<#--<div class="fly-panel">
    <div class="fly-panel-title">
        广告
    </div>
    <div class="fly-panel-main">
        <a href="javascript:void(0);" target="_blank" class="fly-zanzhu" style="background-color: #393D49;">
            虚席以待
        </a>
    </div>
</div>-->
    <div class="fly-panel">
        <div class="fly-panel-title">
            公众号
        </div>
        <div class="fly-panel-main" style="padding: 20px 0; text-align: center;">
            <img src="/assets/images/qrcode_wechat_8.jpg">
            <p style="position: relative; color: #666;">微信扫码关注 书院时光 公众号</p>
        </div>
    </div>
<#--
<div class="fly-panel fly-link">
    <h3 class="fly-panel-title">友情链接</h3>
 <dl class="fly-panel-main">
     <dd>
         <a href="https://gitee.com/e2edour" target="_blank">码云</a>
     <dd>
     <dd>
         <a href="http://www.golearning.cn/" target="_blank">Golearning</a>
     <dd>
     <dd>
     &lt;#&ndash;<a href="mailto:xianxin@layui-inc.com?subject=%E7%94%B3%E8%AF%B7Fly%E7%A4%BE%E5%8C%BA%E5%8F%8B%E9%93%BE"
        class="fly-link">申请友链</a>&ndash;&gt;
         <a href="javascript:void (0);"
            class="fly-link">申请友链</a>
     <dd>
 </dl>
        <dl class="fly-panel-main">
            <dd>
                <a href="javascript:void(0)" class="fly-link">申请友链</a>
            <dd>
        </dl>
    </div>
-->
</div>
<script type="text/javascript" src="${context.contextPath}/assets/js/views/_include/right.js"></script>