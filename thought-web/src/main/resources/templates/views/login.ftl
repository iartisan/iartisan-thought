<title>${_title!''}</title>
<div class="layui-container fly-marginTop">
    <div class="fly-panel fly-panel-user" pad20>
        <div class="layui-tab layui-tab-brief" lay-filter="user">
            <ul class="layui-tab-title">
                <li class="layui-this">登入</li>
                <li><a href="${context.contextPath}/reg/addDataPage">注册</a></li>
            </ul>
            <div class="layui-form layui-tab-content" id="LAY_ucm" style="padding: 20px 0;">
                <div class="layui-tab-item layui-show">
                    <div class="layui-form layui-form-pane">
                        <form method="post" action="${context.contextPath}/authenticate">
                            <div class="layui-form-item">
                                <label for="L_email" class="layui-form-label">邮箱</label>
                                <div class="layui-input-inline">
                                    <input type="text" name="email" required lay-verify="required"
                                           autocomplete="off" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="L_pass" class="layui-form-label">密码</label>
                                <div class="layui-input-inline">
                                    <input type="password" name="pass" required lay-verify="required"
                                           autocomplete="off" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="L_vercode" class="layui-form-label">验证码</label>
                                <div class="layui-input-inline">
                                    <input type="text" name="vercode" required lay-verify="required"
                                           placeholder="请输入验证码" autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-form-mid">
                                    <span style="color: #c00;">
                                        <img name="vercode" src="${context.contextPath}/captcha"
                                             style="height:50%;width: 50%;" onclick="this.src=this.src">
                                    </span>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <button class="layui-btn" lay-filter="btnLogin" lay-submit>立即登录</button>
                                <span style="padding-left:20px;">
                                <#--<a href="forget.html">忘记密码？</a>-->
                                </span>
                            </div>
                        <#--  <div class="layui-form-item fly-form-app">
                              <span class="layui-bg-orange">第三方互联登录，以后推出</span>
                          </div>-->
                            <div class="layui-form-mid layui-word-aux">第三方互联登录还在开发中，敬请期待</div>
                        <#--<div class="layui-form-item fly-form-app">
                            <span>或者使用社交账号登入</span>
                            <a href="" onclick="layer.msg('正在通过QQ登入', {icon:16, shade: 0.1, time:0})"
                               class="iconfont icon-qq" title="QQ登入"></a>
                            <a href="" onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})"
                               class="iconfont icon-weibo" title="微博登入"></a>
                        </div>-->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="${context.contextPath}/assets/js/views/login.js"></script>