<title>基本设置</title>
<div class="layui-tab layui-tab-brief" lay-filter="mine">
    <ul class="layui-tab-title">
        <li class="layui-this" lay-id="info">我的资料</li>
        <li lay-id="avatar">头像</li>
        <li lay-id="pass">密码</li>
    <#--<li lay-id="bind">帐号绑定</li>-->
    </ul>
    <div class="layui-tab-content" style="padding: 20px 0;">
        <div class="layui-form layui-form-pane layui-tab-item layui-show">
            <form method="post">
                <div class="layui-form-item">
                    <label for="L_email" class="layui-form-label">邮箱</label>
                    <div class="layui-input-inline">
                        <input type="text" autocomplete="off"
                               value="${data.custAccount}" class="layui-input layui-disabled" disabled>
                    </div>
                    <div class="layui-form-mid layui-word-aux">暂不支持修改</div>
                </div>
                <div class="layui-form-item">
                    <label for="L_username" class="layui-form-label">用户名</label>
                    <div class="layui-input-inline">
                        <input type="text" name="custNickName" required lay-verify="required"
                               autocomplete="off" value="${data.custUserName}" class="layui-input layui-disabled"
                               disabled>
                    </div>
                    <div class="layui-form-mid layui-word-aux">暂不支持修改</div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">性别</label>
                    <#assign mChecked>
                        <#if (data.custGender) ??>
                            <#if (data.custGender)='M'>checked</#if>
                        </#if>
                    </#assign>
                    <#assign fChecked>
                        <#if (data.custGender) ??>
                            <#if (data.custGender)='F'>checked</#if>
                        </#if>
                    </#assign>
                    <div class="layui-input-inline">
                        <input type="radio" name="custGender" value="M" title="男" ${mChecked!''}>
                        <input type="radio" name="custGender" value="F" title="女" ${fChecked!''}>
                    </div>
                </div>
                <div class="layui-form-item layui-form-text">
                    <label for="L_sign" class="layui-form-label">签名</label>
                    <div class="layui-input-block">
                        <textarea placeholder="随便写些什么刷下存在感" name="sign" autocomplete="off"
                                  class="layui-textarea" style="height: 80px;">${data.sign!''}</textarea>
                    </div>
                </div>
                <div class="layui-form-item">
                    <button class="layui-btn" key="set_mine" lay-filter="*" lay-submit>确认修改</button>
                </div>
        </div>

        <div class="layui-form layui-form-pane layui-tab-item">
            <div class="layui-form-item">
                <div class="avatar-add">
                    <p>建议尺寸168*168，支持jpg、png、gif，最大不能超过50KB</p>
                    <button type="button" class="layui-btn upload-img">
                        <i class="layui-icon">&#xe67c;</i>上传头像
                    </button>
                    <img src="<#if (data.avatar) ?? >${context.contextPath}/oss/rest/download/${data.avatar}<#else>/assets/images/avatar/default.png</#if>">
                    <span class="loading"></span>
                </div>
            </div>
        </div>

        <div class="layui-form layui-form-pane layui-tab-item">
            <form method="post">
                <div class="layui-form-item">
                    <label for="L_nowpass" class="layui-form-label">当前密码</label>
                    <div class="layui-input-inline">
                        <input type="password" name="oldPwd" required lay-verify="required"
                               autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="L_pass" class="layui-form-label">新密码</label>
                    <div class="layui-input-inline">
                        <input type="password" id="newPwd" name="newPwd" required lay-verify="required"
                               autocomplete="off"
                               class="layui-input">
                    </div>
                    <div class="layui-form-mid layui-word-aux">6到16个字符</div>
                </div>
                <div class="layui-form-item">
                    <label for="L_repass" class="layui-form-label">确认密码</label>
                    <div class="layui-input-inline">
                        <input type="password" id="confirmNewPwd" name="confirmNewPwd" required
                               lay-verify="required|confirmPwd"
                               autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <button class="layui-btn" key="resetPwd" lay-filter="*" lay-submit>确认修改</button>
                </div>
            </form>
        </div>

    <#--        <div class="layui-form layui-form-pane layui-tab-item">
                <ul class="app-bind">
                    <li class="fly-msg app-havebind">
                        <i class="iconfont icon-qq"></i>
                        <span>已成功绑定，您可以使用QQ帐号直接登录Fly社区，当然，您也可以</span>
                        <a href="javascript:;" class="acc-unbind" type="qq_id">解除绑定</a>

                        <!-- <a href="" onclick="layer.msg('正在绑定微博QQ', {icon:16, shade: 0.1, time:0})" class="acc-bind" type="qq_id">立即绑定</a>
                        <span>，即可使用QQ帐号登录Fly社区</span> &ndash;&gt;
                    </li>
                    <li class="fly-msg">
                        <i class="iconfont icon-weibo"></i>
                        <!-- <span>已成功绑定，您可以使用微博直接登录Fly社区，当然，您也可以</span>
                        <a href="javascript:;" class="acc-unbind" type="weibo_id">解除绑定</a> &ndash;&gt;

                        <a href="" class="acc-weibo" type="weibo_id"
                           onclick="layer.msg('正在绑定微博', {icon:16, shade: 0.1, time:0})">立即绑定</a>
                        <span>，即可使用微博帐号登录Fly社区</span>
                    </li>
                </ul>
            </div>-->
    </div>
</div>
<script type="text/javascript" src="${context.contextPath}/assets/js/views/my/my_set.js"></script>