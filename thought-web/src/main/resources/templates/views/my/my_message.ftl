<title>我的消息</title>
<div class="layui-tab layui-tab-brief" lay-filter="user" id="LAY_msg" style="margin-top: 15px;">
    <div id="LAY_minemsg" style="margin-top: 10px;">
        <ul class="mine-msg">
        <#if data ?? >
            <button class="layui-btn layui-btn-danger" id="btnReadAll">已读全部消息</button>
            <#list data as value>
                <li data-id="${value.id!''}">
                    <blockquote class="layui-elem-quote">
                    ${value.content}
                    </blockquote>
                    <p>
                        <span><@tagcode code="datetime" value="${value.createTime?datetime}"> </@tagcode></span>
                        <a href="javascript:;" class="layui-btn layui-btn-small layui-btn-danger delete" attr-data="${value.id!''}">已读</a></p>
                </li>
            </#list>
        <#else >
            <div class="fly-none">您暂时没有最新消息</div>
        </#if>
        </ul>
    </div>
</div>
<script type="text/javascript" src="${context.contextPath}/assets/js/views/my/my_message.js"></script>