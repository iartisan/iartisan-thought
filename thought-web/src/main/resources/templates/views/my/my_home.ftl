<title>我的主页</title>
<div class="fly-home fly-panel" style="background-image: url();margin-top: 10px;">
    <img src="<#if (data.avatar)>/oss/rest/download/${data.avatar!''}<#else >/assets/images/avatar/default.png</#if>">
    <h1>
    ${data.custNicName}
    <#assign genderIcon>
        <#if (data.custGender) ?? >
            <#if (data.custGender)='M'>
                <i class="iconfont icon-nan"></i>
            <#elseif (data.custGender)='F'>
                <i class="iconfont icon-nv"></i>
            </#if>
        </#if>
    </#assign>
    ${genderIcon!''}
    </h1>
    <p class="fly-home-info">
    </p>
    <p class="fly-home-sign">签名：${data.sign!''}</p>
    <div class="fly-sns">
    <#if _user ?? >
        <#if (_user.userId)!=(data.custId) >
        <#elseif (_user.userId)==(data.custId) >
            <a href="/my/set" class="layui-btn layui-btn-radius fly-imActive">去编辑个人信息</a>
        </#if>
    </#if>
    </div>
</div>
