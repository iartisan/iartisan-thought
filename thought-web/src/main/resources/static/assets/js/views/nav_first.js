layui.config({
    base: "/assets/js/lib/"
}).use(['util', 'element', 'router'], function () {
    var util = layui.util, $ = layui.jquery, router = layui.router;
    //固定Bar
    util.fixbar({
        bar1: '&#xe642;',
        bgcolor: '#1E9FFF',
        click: function (type) {
            if (type === 'bar1') {
                location.href = '/topic/management/addDataPage';
            }
        }
    });
    var requestURL = {
        getUnreadCount: "/userMessage/query/getUnreadCount"
    };
    //加载未读消息
    var online = $('#_online').val();
    var elemUser = $('.fly-nav-user');
    if (typeof online != 'undefined') {
        router.post({
            url: requestURL.getUnreadCount,
            success: function (res) {
                if (res.data > 0) {
                    var msg = $('<a class="fly-nav-msg" href="javascript:;">' + res.data + '</a>');
                    elemUser.append(msg);
                    msg.on('click', function () {
                        //跳转到消息列表页面
                        router.location("/my/message");
                    });
                    layer.tips('你有 ' + res.data + ' 条未读消息', msg, {
                        tips: 3, tipsMore: true, fixed: true
                    });
                    msg.on('mouseenter', function () {
                        layer.closeAll('tips');
                    })
                }
            }
        });
    }
});