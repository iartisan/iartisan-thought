//一般直接写在一个js文件中
layui.use(['form', 'router', 'md5', 'layer'], function () {
    var $ = layui.jquery,
        router = layui.router,
        form = layui.form,
        layer = layui.layer;
    var urls = {reg: "/reg/addData"};
    form.on("submit(*)", function (data) {
        //密码加密
        data.field["custPwd"] = $.md5(data.field["custPwd"]);
        router.post({
            url: urls.reg,
            data: data.field,
            success: function (res) {
                layer.msg(res.message, {icon: 1, time: 2000}, function () {
                    location.href = "/login";
                });
            }
        });
        return false;
    });
});