//一般直接写在一个js文件中
layui.config({
    base: "/assets/js/lib/"
}).use(['layer', 'form', 'router'], function () {
    var $ = layui.jquery, router = layui.router, layer = layui.layer;

    var urls = {
        setReadByCustId: "/userMessage/management/setReadByCustId",
        setReadById: "/userMessage/management/setReadById"
    };
    //全部删除
    $("#btnReadAll").on("click", function () {
        layer.confirm('确定标记全部已读吗？', function (index) {
            layer.close(index);
            router.post({
                url: urls.setReadByCustId,
                success: function () {
                    location.reload();
                }
            });
        });
    });
    //全部删除
    $(".delete").on("click", function () {
        var data=$(this).attr("attr-data");
        router.post({
            url: urls.setReadById,
            data: {id:data},
            success: function () {
                location.reload();
            }
        });
    });
});