layui.config({
    base: "/assets/js/lib/"
}).use(['layer', 'form', 'fly', 'router','laydate'], function () {
    var $ = layui.jquery,
        layer = layui.layer,
        form = layui.form,
        router = layui.router,
        laydate=layui.laydate,
        fly = layui.fly;

    //加载编辑器
    fly.layEditor({
        elem: '.fly-editor'
    });
    laydate.render({
        elem: '#startDate',
        type: 'date'
    });
    laydate.render({
        elem: '#startTime',
        type: 'time'
    });
    var category = {
        fleaMarket: "fleaMarket",
        carpool: "carpool"
    };

    var urls = {
        upload: "/oss/rest/upload",
        download: "/oss/rest/download"
    };

    function renderFleaMarket(data) {
        var newData = {};
        newData.productName = data.field["productName"];
        newData.productPrice = data.field["productPrice"];
        newData.contactPhone = data.field["contactPhone"];
        newData.productDesc = data.field["productDesc"];
        newData.productPic = data.field["productPic"];
        return newData;
    }

    function renderCar(data) {
        var newData = {};
        newData.startLocation = data.field["startLocation"];
        newData.destination = data.field["destination"];
        newData.leftSeats = data.field["leftSeats"];
        newData.startDate = data.field["startDate"];
        newData.startTime = data.field["startTime"];
        return newData;
    }

    form.on('submit(btnSubmit)', function (data) {
        var action = $(data.form).attr('action'), button = $(data.elem);
        //内容转义
        var content = data.field.content;
        //如果是跳蚤市场 手拼一个content
        if (data.field["categoryKey"] == category.fleaMarket) {
            content = JSON.stringify(renderFleaMarket(data));
        } else if (data.field["categoryKey"] == category.carpool) {
            content = JSON.stringify(renderCar(data));
        } else {
            content = /^{html}/.test(content) ? content.replace(/^{html}/, '') : fly.content(content);
        }
        data.field.content = content;
        router.post({
            url: action,
            data: data.field,
            success: function (res) {
                location.href = res.message
            }
        });
        return false;
    });

    var dom = {
        fleaMarketForm: $('#fleaMarketForm'),
        carpoolForm: $('#carpoolForm'),
        contentDiv: $("#contentDiv"),
        content: $("#content"),
        tips: {
            tips: 1,
            maxWidth: 250,
            time: 5000
        }
    };
    form.on('select(column)', function (obj) {
        var value = obj.value;
        dom.fleaMarketForm.addClass('layui-hide');
        if (value === 'fleaMarket') {
            showFleaMarketForm(obj);
        } else if (value == 'carpool') {
            showCarpoolForm(obj);
        } else {
            showContent();
        }
    });

    function showFleaMarketForm(obj) {
        layer.tips('请输入商品信息，注意本网站只提供发布，不对发布信息进行确认，不涉及交易，不对此过程的任何问题承担责任（算是免责声明！）', obj.othis, dom.tips);
        dom.fleaMarketForm.find(":input").not("[type='file']").not("[type='hidden']").attr("lay-verify", "required");
        dom.fleaMarketForm.removeClass('layui-hide');
        dom.fleaMarketForm.find("input[name='productPrice']").attr("lay-verify", "required|number");
        hideCarpoolForm();
        hideContent();
    }

    function showCarpoolForm(obj) {
        layer.tips('请输入拼车信息，注意本网站只提供发布，不对发布信息进行确认，不涉及交易，不对此过程的任何问题承担责任（算是免责声明！）', obj.othis, dom.tips);
        dom.carpoolForm.removeClass("layui-hide");
        dom.carpoolForm.find("input").attr("lay-verify", "required");
        hideContent();
        hideFleaMarketForm();
    }

    function showContent() {
        dom.contentDiv.removeClass("layui-hide");
        dom.content.attr("lay-verify", "required");
        hideCarpoolForm();
        hideFleaMarketForm();
    }

    function hideContent() {
        dom.contentDiv.addClass("layui-hide");
        dom.content.removeAttr("lay-verify");
    }

    function hideFleaMarketForm() {
        dom.fleaMarketForm.addClass("layui-hide");
        dom.fleaMarketForm.find("input").removeAttr("lay-verify");
    }

    function hideCarpoolForm() {
        dom.carpoolForm.addClass("layui-hide");
        dom.carpoolForm.find("input").removeAttr("lay-verify");
    }

    if ($('.upload-img')[0]) {
        layui.use('upload', function (upload) {

            upload.render({
                elem: '.upload-img',
                url: urls.upload,
                size: 500,
                before: function () {

                },
                done: function (res) {
                    if (res.code == '000000') {
                        $("#previewImg").attr("src", urls.download + "/" + res.message);
                        $("#productPic").val(res.message);
                    }
                },
                error: function () {

                }
            });
        });
    }
});