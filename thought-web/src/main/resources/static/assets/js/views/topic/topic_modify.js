layui.config({
    base: "/assets/js/lib/"
}).use(['layer', 'form', 'fly', 'router'], function () {
    var $ = layui.jquery,
        layer = layui.layer,
        form = layui.form,
        router = layui.router,
        fly = layui.fly;

    //加载编辑器
    fly.layEditor({
        elem: '.fly-editor'
    });


    form.on('submit(btnSubmit)', function (data) {
        var action = $(data.form).attr('action'), button = $(data.elem);
        //内容转义
        var content = data.field.content;
        content = /^{html}/.test(content) ? content.replace(/^{html}/, '') : fly.content(content);
        data.field.content = content;
        router.post({
            url: action,
            data: data.field,
            success: function (res) {
                location.href = res.message
            }
        });
        return false;
    });
    $("#content").html(fly.content($("#content").html()));

});