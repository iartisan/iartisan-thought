layui.config({
    base: "/assets/js/lib/"
}).use(['laytpl', 'router'], function () {
    var laytpl = layui.laytpl,
        $ = layui.jquery,
        router = layui.router;

    var requestURL = {
        queryWarmChannels: "/warmChannel/query/queryListData",
        getHotTopics: "/topic/query/getHotTopics"
    };
    //本周热议
    router.post({
        url: requestURL.getHotTopics,
        success: function (res) {
            var tpl = $("#_hotTopicScript").html(), view = $("#_hotTopicsDt");
            laytpl(tpl).render(res, function (html) {
                view.html(html)
            });
        }
    });
    //温馨通道
    router.post({
        url: requestURL.queryWarmChannels,
        success: function (res) {
            var tpl = $("#_warmChannel").html(), view = $("#_warmChannelUl");
            laytpl(tpl).render(res, function (html) {
                view.html(html)
            })
        }
    });
});