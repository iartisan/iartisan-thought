//一般直接写在一个js文件中
layui.config({
    base: "/assets/js/lib/"
}).use(['layer', 'form', 'router', 'fly'], function () {
    var $ = layui.jquery,
        layer = layui.layer,
        form = layui.form,
        router = layui.router,
        fly = layui.fly;

    //加载编辑器
    fly.layEditor({
        elem: '.fly-editor'
    });

    var urls = {
        accept: "/comment/management/accept",
        topicAction: "/topic/management/action",
        addWarmChannel: "/warmChannel/management/addData",
        collect: "/topic/management/collect",
        zan: "/comment/management/zan",
        modifyTopic:"/topic/management/modifyDataPage"
    };

    var gather = {}, dom = {jieda: $('#jieda'), content: $('#L_content'), jiedaCount: $('#jiedaCount')};

    $('.jieda-reply span').on('click', function () {
        var othis = $(this), type = othis.attr('type');
        gather.jiedaActive[type].call(this, othis.parents('li'));
    });
    //解答操作
    gather.jiedaActive = {
        reply: function (li) { //回复
            var val = dom.content.val();
            var aite = '@' + li.find('.fly-detail-user cite').text().replace(/\s/g, '');
            dom.content.focus()
            if (val.indexOf(aite) !== -1) return;
            dom.content.val(aite + ' ' + val);
        },
        accept: function (li) { //采纳
            layer.confirm('是否采纳该回答为最佳答案？', function (index) {
                layer.close(index);
                router.post({
                        url: urls.accept,
                        data: {commentId: li.data('id')},
                        success: function () {
                            $('.jieda-accept').remove();
                            li.addClass('jieda-daan');
                            li.find('.detail-about').append('<i class="iconfont icon-caina" title="最佳答案"></i>');
                            $("#topidIsEnd").html("已结");
                        }
                    }
                );
            });
        },
        zan: function (li) { //赞
            var othis = $(this), ok = othis.hasClass('zanok');
            router.post({
                url: urls.zan,
                data: {commentId: li.data('id')},
                success: function (res) {
                    //zan 成功后 添加zanok 样式
                    var zans = othis.find('em').html() | 0;
                    othis[ok ? 'removeClass' : 'addClass']('zanok');
                    othis.find('em').html(ok ? (--zans) : (++zans));
                }
            });
        }
    };
    //提交
    form.on('submit(btnSubmit)', function (data) {
        var action = $(data.form).attr('action'), button = $(data.elem);
        //内容转义
        var content = data.field.content;
        content = /^{html}/.test(content) ? content.replace(/^{html}/, '') : fly.content(content);
        data.field.content = content;
        router.post({
            url: action,
            data: data.field,
            success: function (res) {
                location.href = res.message;
            }
        });
        return false;
    });
    $("#collect").on("click", function () {
        var that = $(this);
        var topicId = that.attr("data-id");
        router.post({
            url: urls.collect,
            data: {"topicId": topicId},
            success: function () {
                var text = (that.text().trim() === "收藏" ? "取消收藏" : "收藏");
                that.text(text);
            }
        });
        return false;
    });
    $('body').on('click', '.jie-admin', function () {
        var othis = $(this), type = othis.attr('type');
        adminAction[type] && adminAction[type].call(this, othis.parent());
    });
    //管理员操作
    var adminAction = {
        //编辑
        edit:function (div) {
            layer.confirm('确认编辑贴子么？', function (index) {
                layer.close(index);
                location.href = urls.modifyTopic+"/"+div.data('id');
            });
        },
        //删除
        del: function (div) {
            layer.confirm('确认删除贴子么？', function (index) {
                layer.close(index);
                router.post({
                        url: urls.topicAction,
                        data: {topicId: div.data('id'), action: "del"},
                        success: function () {
                            location.href = '/index';
                        }
                    }
                );
            });
        },
        //置顶
        top: function (div) {
            router.post({
                    url: urls.topicAction,
                    data: {topicId: div.data('id'), action: "top"},
                    success: function () {
                        location.reload();
                    }
                }
            );
        },
        //加精
        fine: function (div) {
            router.post({
                    url: urls.topicAction,
                    data: {topicId: div.data('id'), action: "fine"},
                    success: function () {
                        location.reload();
                    }
                }
            );
        },
        channel: function (div) {
            router.post({
                    url: urls.addWarmChannel,
                    data: {topicId: div.data('id')},
                    success: function () {
                        location.reload();
                    }
                }
            );
        }
    }
    //定位分页
    if(location.hash){
        var replyTop = $(location.hash).offset().top - 80;
        $('html,body').scrollTop(replyTop);
    }
});