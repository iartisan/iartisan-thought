package org.iartisan.thought.web.parse;

import org.iartisan.runtime.utils.JsonUtil;
import org.iartisan.runtime.utils.MessageUtil;
import org.iartisan.runtime.utils.StringUtils;
import org.iartisan.thought.web.parse.entity.CarPool;
import org.iartisan.thought.web.parse.entity.FleaMarket;

/**
 * <p>
 * 文章解析器
 *
 * @author King
 * @since 2018/5/23
 */
public class TopicParse {

    public static String getFleaMarket(String content) {
        FleaMarket market = JsonUtil.parseObject(content, FleaMarket.class);
        return MessageUtil.format(fleaMarketTemplate, market.getProductName(), market.getProductPrice(),
                "******", market.getProductDesc(),
                !StringUtils.isEmpty(market.getProductPic()) ? "<img style='width:99px;height:99px;' src='/oss/rest/download/" + market.getProductPic() + "'>" : "");
    }

    public static String getCarPool(String content) {
        CarPool carPool = JsonUtil.parseObject(content, CarPool.class);
        return MessageUtil.format(carPoolTemplate, carPool.getStartLocation(), carPool.getDestination(),
                carPool.getStartDate(), carPool.getStartTime(), carPool.getLeftSeats());
    }

    private final static String disclaimer = "<div class='layui-word-aux' style='margin-top:50px;'>" +
            "注意：<br>" +
            "1. 本网站只提供发布，不对发布信息进行确认，不涉及交易，未从中牟利，" +
            "不对此过程的任何问题承担责任（算是免责声明！<i class=\"layui-icon\" style=\"font-size: 20px; color: #FF5722;\">&#xe60c;</i> ）<br>" +
            "2. 出于安全考虑目前并不开放联系方式等敏感字段，沟通可以回贴的方式进行" +
            "</div>";
    private static final String carPoolTemplate = "<table class=\"layui-table\">\n" +
            "<thead><th colspan='2'> 拼车详情</br></th></thead>" +
            "                <tbody>\n" +
            "                <tr>\n" +
            "                    <td nowrap width='15%'><img src='/assets/images/begin.png'/></td>\n" +
            "                    <td>{0}</td>\n" +
            "                </tr>\n" +
            "                <tr>\n" +
            "                    <td><img src='/assets/images/end.png'/></td>\n" +
            "                    <td>{1}</td>\n" +
            "                </tr>\n" +
            "                <tr>\n" +
            "                    <td>发车时间</td>\n" +
            "                    <td>{2} {3}</td>\n" +
            "                </tr>\n" +
            "                <tr>\n" +
            "                    <td>剩余座位</td>\n" +
            "                    <td>{4}</td>\n" +
            "                </tr>\n" +
            "                </tbody>\n" +
            "            </table>" + disclaimer;

    private static final String fleaMarketTemplate =
            "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" class=\"layui-table\">\n" +
                    "<thead><th colspan='2' class='layui-bg-orange'> 商品详情</br></th></thead>" +
                    "                <tbody>\n" +
                    "                <tr>\n" +
                    "                    <td nowrap class='layui-bg-cyan' width='30%'>商品名称</td>\n" +
                    "                    <td>{0}</td>\n" +
                    "                </tr>\n" +
                    "                <tr>\n" +
                    "                    <td class='layui-bg-cyan'>商品价格</td>\n" +
                    "                    <td>{1}</td>\n" +
                    "                </tr>\n" +
                    "                <tr>\n" +
                    "                    <td class='layui-bg-cyan'>联系方式</td>\n" +
                    "                    <td>{2}</td>\n" +
                    "                </tr>\n" +
                    "                <tr>\n" +
                    "                    <td class='layui-bg-cyan'>商品描述</td>\n" +
                    "                    <td>{3}</td>\n" +
                    "                </tr>\n" +
                    "                <tr>\n" +
                    "                    <td class='layui-bg-cyan'>商品图片</td>\n" +
                    "                    <td>{4}</td>\n" +
                    "                </tr>\n" +
                    "                </tbody>\n" +
                    "            </table>" + disclaimer;


}
