package org.iartisan.thought.web.tag.utils;

import java.io.IOException;
import java.io.Writer;
import java.text.MessageFormat;

/**
 * <p>
 * admin
 *
 * @author King
 * @since 2018/1/30
 */
public class AdminTopicDirective {

    private static final String role_admin = "admin";

    private static final String _flag = "Y";

    public static void render(Writer writer, String value, String pattern, String adminPattern, String actionYesText, String actionNoText, String text) throws IOException {
        //空处理 cip admin
        String[] values = value.split("\\|");
        if (values.length > 1) {
            //获取角色
            String role = values[1];
            if (role.equals(role_admin)) {
                String flag = values[0];
                if (_flag.equals(flag)) {
                    writer.write(MessageFormat.format(adminPattern, actionNoText));
                } else {
                    writer.write(MessageFormat.format(adminPattern, actionYesText));
                }
            } else {
                if (_flag.equals(values[0])) {
                    writer.write(MessageFormat.format(pattern, text));
                }
            }
        } else {
            if (values.length > 0) {
                if (_flag.equals(values[0])) {
                    writer.write(MessageFormat.format(pattern, text));
                }
            }
        }
    }
}
