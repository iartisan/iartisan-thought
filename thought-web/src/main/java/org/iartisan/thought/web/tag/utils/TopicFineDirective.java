package org.iartisan.thought.web.tag.utils;

import java.io.IOException;
import java.io.Writer;
import java.text.MessageFormat;

/**
 * <p>
 * fine
 *
 * @author King
 * @since 2017/12/21
 */
public class TopicFineDirective {

    private static final String pattern = "<span class=\"layui-badge layui-bg-red\">{0}</span>";

    private static String admin_pattern = "<span class=\"layui-badge layui-bg-red jie-admin\" type=\"fine\">{0}</span>";

    protected static String actionYesText = "加精";

    protected static String actionNoText = "取消加精";

    protected static String text = "精贴";

    public static void render(Writer writer, String value) throws IOException {
        AdminTopicDirective.render(writer, value, pattern, admin_pattern, actionYesText, actionNoText, text);
    }
}
