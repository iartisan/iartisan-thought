package org.iartisan.thought.web;

import org.iartisan.runtime.env.EnvPropertiesLoader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import java.util.Properties;

/**
 * <p>
 * 启动类
 *
 * @author King
 * @since 2017/10/19
 */
@SpringBootApplication
@ComponentScan(basePackages = {"org.iartisan.thought.web", "org.iartisan.oss"})
public class WebBootRun {

    public static void main(String[] args) {
        Properties properties = EnvPropertiesLoader.loadFile();
        SpringApplication application = new SpringApplication(WebBootRun.class);
        application.setDefaultProperties(properties);
        application.run(args);
    }

}
