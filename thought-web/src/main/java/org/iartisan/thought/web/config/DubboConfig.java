package org.iartisan.thought.web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.stereotype.Component;

/**
 * <p>
 * dubbo config
 *
 * @author King
 * @since 2017/12/8
 */
@Component
@Configuration
@ImportResource({
        "classpath:dubbo-consumer-config.xml"
})
public class DubboConfig {

}
