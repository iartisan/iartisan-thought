package org.iartisan.thought.web.controller;

import org.iartisan.runtime.web.controller.BaseController;

/**
 * <p>
 *
 * @author King
 * @since 2018/5/29
 */
public class MyBaseController extends BaseController {

    protected final static String _topicPageData = "_topicPageData";

    protected final static String _categoryKey = "_categoryKey";

    protected final static String _statusKey = "_statusKey";

}
