package org.iartisan.thought.web.authentication;


import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.subject.PrincipalCollection;
import org.iartisan.runtime.api.res.BaseOneRes;
import org.iartisan.runtime.api.utils.ApiResUtil;
import org.iartisan.runtime.constants.DataStatus;
import org.iartisan.runtime.exception.ApiRemoteException;
import org.iartisan.runtime.web.authentication.AuthenticationService;
import org.iartisan.runtime.web.authentication.RealmBean;
import org.iartisan.runtime.web.contants.WebConstants;
import org.iartisan.runtime.web.utils.WebUtil;
import org.iartisan.thought.server.api.query.CustQueryFacade;
import org.iartisan.thought.server.api.req.CustLoginReq;
import org.iartisan.thought.server.api.res.CustBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * <p>
 * 认证接口服务
 *
 * @author King
 * @since 2017/11/3
 */
@Component
public class AuthenticationServiceImpl extends AuthenticationService {

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        RealmBean realmBean = (RealmBean) principals.getPrimaryPrincipal();
        //用户权限列表
        Set<String> permsSet = getPermissions(realmBean.getUserId());
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.setStringPermissions(permsSet);
        return info;
    }

    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String userName = (String) token.getPrincipal();
        String userPwd = new String((char[]) token.getCredentials());
        try {
            RealmBean realmBean = getRealmBean(userName, userPwd);
            if (realmBean == null) {
                throw new IncorrectCredentialsException("用户名或密码错误,请重新登录");
            }
            //判断用户状态
            WebUtil.getShiroSession().setAttribute(WebConstants._USER, realmBean);
            SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(realmBean, userPwd, getName());
            return info;
        } catch (ApiRemoteException e) {
            throw new IncorrectCredentialsException("用户名或密码错误,请重新登录");
        } catch (AuthenticationException e) {
            throw new IncorrectCredentialsException(e.getMessage());
        }
    }

    @Autowired
    private CustQueryFacade custQueryFacade;

    /**
     * 用户登录
     *
     * @param userName
     * @param userPwd
     * @return
     */
    protected RealmBean getRealmBean(String userName, String userPwd) throws ApiRemoteException {
        CustLoginReq req = new CustLoginReq();
        req.setCustAccount(userName);
        req.setCustPwd(userPwd);
        BaseOneRes<CustBean> res = custQueryFacade.custLogin(req);
        CustBean custBean = ApiResUtil.getBean(res);
        if (custBean.getStatus().equals(DataStatus.M.name())) {
            throw new AuthenticationException("用户未激活，请登录邮件进行激活");
        }
        RealmBean realmBean = new RealmBean();
        realmBean.setUserName(custBean.getCustUserName());
        realmBean.setUserId(custBean.getCustId());
        realmBean.setRole(custBean.getCustType());
        realmBean.setAvatar(custBean.getAvatar());
        return realmBean;
    }

    /**
     * 获取用户权限列表
     *
     * @param userId
     * @return
     */
    protected Set<String> getPermissions(String userId) {
        return null;
    }
}
