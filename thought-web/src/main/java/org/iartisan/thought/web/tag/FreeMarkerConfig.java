package org.iartisan.thought.web.tag;

import freemarker.template.Configuration;
import org.iartisan.runtime.bean.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * <p>
 * freemaker config
 *
 * @author King
 * @since 2017/12/20
 */
@Component
public class FreeMarkerConfig {
    @Autowired
    private Configuration configuration;

    @Autowired
    private TagCodeDirective tagDirective;

    @Autowired
    private PageDirective pageDirective;

    @PostConstruct
    public void setSharedVariable() {
        configuration.setSharedVariable("pageCode", pageDirective);
        configuration.setSharedVariable("tagcode", tagDirective);
    }
}
