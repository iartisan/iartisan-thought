package org.iartisan.thought.web.integration.management;

import org.iartisan.runtime.api.utils.ApiResUtil;
import org.iartisan.runtime.exception.ApiRemoteException;
import org.iartisan.thought.server.api.management.UserMessageManagementFacade;
import org.iartisan.thought.server.api.req.BaseCustReq;
import org.iartisan.thought.server.api.req.BaseIdReq;
import org.iartisan.thought.server.api.req.CustRestPwdReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author King
 * @since 2018/2/7
 */
@Service
public class UserMessageManagementClient {

    @Autowired
    private UserMessageManagementFacade userMessageManagementFacade;

    /**
     * 删除消息
     *
     * @return
     */
    public void deleteByCustId(String custId) throws ApiRemoteException {
        BaseCustReq req = new BaseCustReq();
        req.setCustId(custId);
        ApiResUtil.getResult(userMessageManagementFacade.deleteByCustId(req));
    }

    /**
     * 删除消息
     *
     * @return
     */
    public void deleteById(Integer id) throws ApiRemoteException {
        BaseIdReq req = new BaseIdReq();
        req.setId(id);
        ApiResUtil.getResult(userMessageManagementFacade.deleteById(req));
    }

    public void setReadById(Integer id) throws ApiRemoteException {
        BaseIdReq req = new BaseIdReq();
        req.setId(id);
        ApiResUtil.getResult(userMessageManagementFacade.setReadById(req));
    }

    public void setReadByCustId(String custId) throws ApiRemoteException {
        BaseCustReq req = new BaseCustReq();
        req.setCustId(custId);
        ApiResUtil.getResult(userMessageManagementFacade.setReadByCustId(req));
    }


}
