package org.iartisan.thought.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * <p>
 * 装饰器
 *
 * @author King
 * @since 2017/12/7
 */
@Controller
public class DecoratorContoller {
    private static final String prefix = "_include/";

    @GetMapping("doDecoratorHomeIndex")
    public String doDecoratorHomeIndex() {
        return prefix + "decorator_home";
    }

    @GetMapping("doDecoratorFirstNavIndex")
    public String doDecoratorFirstNavIndex() {
        return prefix + "decorator_first_nav";
    }

    /**
     * 个人中心 decorator
     *
     * @return
     */
    @GetMapping("doDecoratorMy")
    public String doDecoratorMy() {
        return prefix + "decorator_my";
    }
}
