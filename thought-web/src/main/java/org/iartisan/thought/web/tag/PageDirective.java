package org.iartisan.thought.web.tag;

import com.sun.media.sound.SoftTuning;
import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import org.iartisan.runtime.bean.Page;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Writer;
import java.text.MessageFormat;
import java.util.Map;

/**
 * <p>
 * 分页
 *
 * @author King
 * @since 2017/12/20
 */
@Component
public class PageDirective implements TemplateDirectiveModel {


    private static final String urlPattern = "<a href=\"{0}?currPage={1}\">{2}</a>";

    private static final String currentPattern = "<span class='laypage-curr'>{0}</span>";

    private static final String ellipsis = "<span>…</span>";

    private static final String last_page_urlPattern = "<a href=\"{0}?currPage={1}\" class='laypage-last'>尾页</a>";

    private static final String next_page_urlPattern = "<a href=\"{0}?currPage={1}\" class='laypage-next'>下一页</a>";

    private static final String pre_page_urlPattern = "<a href=\"{0}?currPage={1}\" class='laypage-prev'>上一页</a>";

    private static final String start_page_urlPattern = "<a href=\"{0}?currPage={1}\" class='laypage-first'>首页</a>";

    private static int groups = 5;//默认值

    @Override
    public void execute(Environment environment, Map map, TemplateModel[] templateModels, TemplateDirectiveBody templateDirectiveBody) throws TemplateException, IOException {
        Writer out = environment.getOut();
        int currPage = Integer.parseInt(map.get("currPage").toString());
        int totalPage = Integer.parseInt(map.get("totalPage").toString());
        String url = map.get("url").toString();
        StringBuffer buffer = new StringBuffer();
        buffer.append(prePage(url, currPage, totalPage));
        buffer.append(first(url, currPage, totalPage));
        buffer.append(preEllipsis(url, currPage, totalPage));
        buffer.append(groups(url, currPage, totalPage));
        buffer.append(nextEllipsis(url, currPage, totalPage));
        buffer.append(last(url, currPage, totalPage));
        buffer.append(nextPage(url, currPage, totalPage));
        out.write(buffer.toString());
        templateDirectiveBody.render(out);
    }


    //上一页
    public String prePage(String url, int current, int total) {
        if (current > 0 && current != 1) {
            return MessageFormat.format(pre_page_urlPattern, url, current - 1);
        }
        return "";
    }

    //首页
    public String first(String url, int current, int total) {
        if (current >= groups - 1) {
            return MessageFormat.format(start_page_urlPattern, url, 1);
        }
        return "";
    }

    //前省略号Previous ellipsis
    public String preEllipsis(String url, int current, int total) {
        if (current >= groups - 1) {
            return ellipsis;
        }
        return "";
    }

    //连续的一段
    public String groups(String url, int current, int total) {
        double halve = Math.floor((groups - 1) / 2);
        int distance = (int) (current - halve);
        int start = distance >= 1 ? distance : 1;
        distance = (int) (current + halve);
        int end;
        if (current <= 2 && current<total-1) {
            end = groups;
        } else {
            end = distance < total ? distance : total;
        }
        StringBuffer buffer = new StringBuffer();
        for (; start <= end; start++) {
            if (start == current) {
                buffer.append(MessageFormat.format(currentPattern, current));
            } else {
                buffer.append(MessageFormat.format(urlPattern, url, start, start));
            }
        }
        return buffer.toString();
    }

    //后省略号Next  ellipsis
    public String nextEllipsis(String url, int current, int total) {
        double halve = Math.floor((groups - 1) / 2);
        if (current + halve < total) {
            return ellipsis;
        }
        return "";
    }

    //后省略号Next  ellipsis
    public String last(String url, int current, int total) {
        double halve = Math.floor((groups - 1) / 2);
        if (current + halve < total) {
            return MessageFormat.format(last_page_urlPattern, url, total);
        }
        return "";
    }

    //下一页
    public String nextPage(String url, int current, int total) {
        if (current < total) {
            return MessageFormat.format(next_page_urlPattern, url, current + 1);
        }
        return "";
    }
}
