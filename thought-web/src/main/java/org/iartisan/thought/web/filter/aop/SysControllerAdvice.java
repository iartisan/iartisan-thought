package org.iartisan.thought.web.filter.aop;

import org.apache.shiro.authz.AuthorizationException;
import org.iartisan.runtime.exception.ApiRemoteException;
import org.iartisan.runtime.web.WebR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author King
 * @since 2018/3/19
 */
@RestControllerAdvice(basePackages = {"org.iartisan.thought.web.controller"})
@ResponseBody
public class SysControllerAdvice {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @ExceptionHandler(value = ApiRemoteException.class)
    public WebR doNotAllowedException(ApiRemoteException ex) {
        logger.error("ApiRemoteException:{}", ex.getMessage());
        WebR r = new WebR();
        r.isError(ex.getMessage());
        return r;
    }

    @ResponseBody
    @ExceptionHandler(value = AuthorizationException.class)
    public WebR doAuthorizationException(AuthorizationException ex) {
        WebR r = new WebR();
        r.isError(ex.getMessage());
        return r;
    }
}
