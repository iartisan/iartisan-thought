package org.iartisan.thought.web.controller.my;

import org.iartisan.runtime.web.WebR;
import org.iartisan.runtime.web.contants.ReqContants;
import org.iartisan.runtime.web.controller.BaseController;
import org.iartisan.thought.server.api.req.CustModifyReq;
import org.iartisan.thought.server.api.req.CustRestPwdReq;
import org.iartisan.thought.web.integration.management.CustManagementClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 个人中心设置
 *
 * @author King
 * @since 2017/12/19
 */
@Controller
@RequestMapping("my")
public class MyManagementController extends BaseController {


    @Autowired
    private CustManagementClient custManagementClient;

    @ResponseBody
    @PostMapping(value = "setAvatar")
    public WebR setAvatar(String avatar) {
        WebR r = new WebR();
        try {
            custManagementClient.setAvatar(getCustId(), avatar);
        } catch (Exception e) {
            r.isError(e.getMessage());
        }
        return r;
    }

    //修改个人信息
    @ResponseBody
    @PostMapping(value = ReqContants.REQ_MODIFY_DATA)
    public WebR modifyData(CustModifyReq req) {
        req.setCustId(getCustId());
        WebR r = new WebR();
        try {
            custManagementClient.modifyCustData(req);
        } catch (Exception e) {
            r.isError(e.getMessage());
        }
        return r;
    }

    //修改个人信息
    @ResponseBody
    @PostMapping(value ="resetPwd")
    public WebR resetPwd(CustRestPwdReq req) {
        req.setCustId(getCustId());
        WebR r = new WebR();
        try {
            custManagementClient.resetPwd(req);
        } catch (Exception e) {
            r.isError(e.getMessage());
        }
        return r;
    }
}
