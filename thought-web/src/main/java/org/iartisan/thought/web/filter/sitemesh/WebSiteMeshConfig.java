package org.iartisan.thought.web.filter.sitemesh;

import org.iartisan.runtime.web.contants.ReqContants;
import org.sitemesh.builder.SiteMeshFilterBuilder;
import org.sitemesh.config.ConfigurableSiteMeshFilter;
import org.springframework.context.annotation.Configuration;

import javax.servlet.annotation.WebFilter;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *
 * @author King
 * @since 2017/12/7
 */
@Configuration
@WebFilter("/")
public class WebSiteMeshConfig extends ConfigurableSiteMeshFilter {

    private static final List<String> doDecoratorFirstNavList = new ArrayList<>();

    private static final List<String> doDecoratorMyList = new ArrayList<>();

    private static final List<String> doDecoratorHomeList = new ArrayList<>();

    private static final String doDecoratorHomeIndex = "/doDecoratorHomeIndex";

    private static final String doDecoratorFirstNavIndex = "/doDecoratorFirstNavIndex";

    private static final String doDecoratorMy = "/doDecoratorMy";

    static {
        doDecoratorFirstNavList.add("/topic/management/addDataPage");
        doDecoratorFirstNavList.add("/my/home");
        doDecoratorFirstNavList.add(ReqContants.REQ_LOGIN);
        doDecoratorFirstNavList.add("/500");
        doDecoratorFirstNavList.add("/error");
        doDecoratorFirstNavList.add("/reg/" + ReqContants.REQ_ADD_DATA_PAGE);

        doDecoratorMyList.add("/my" + ReqContants.REQ_INDEX);
        doDecoratorMyList.add("/my/message");
        doDecoratorMyList.add("/my/set");

        doDecoratorHomeList.add("/**");
        doDecoratorHomeList.add("/index");
        doDecoratorHomeList.add("/topic/query/queryDetailData/*");
        doDecoratorHomeList.add("/topic/query/" + ReqContants.REQ_QUERY_PAGE_DATA + "/*");
    }

    @Override
    protected void applyCustomConfiguration(SiteMeshFilterBuilder builder) {
        for (String s : doDecoratorHomeList) {
            builder.addDecoratorPath(s, doDecoratorHomeIndex);
        }

        for (String s : doDecoratorFirstNavList) {
            builder.addDecoratorPath(s, doDecoratorFirstNavIndex);
        }

        for (String s : doDecoratorMyList) {
            builder.addDecoratorPath(s, doDecoratorMy);
        }
    }
}
