package org.iartisan.thought.web.tag.utils;

import java.io.IOException;
import java.io.Writer;

/**
 * <p>
 * datetime
 *
 * @author King
 * @since 2017/12/21
 */
public class TopicEndDirective {


    public static void render(Writer writer, String value) throws IOException {
        if ("Y".equals(value)) {
            writer.write("已结");
        }else {
            writer.write("未结");
        }
    }
}
