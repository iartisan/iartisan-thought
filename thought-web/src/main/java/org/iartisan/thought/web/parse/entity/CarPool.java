package org.iartisan.thought.web.parse.entity;

/**
 * <p>
 * 拼车
 *
 * @author King
 * @since 2018/5/30
 */
public class CarPool {

    private String startLocation;

    private String destination;

    private String leftSeats;

    private String startDate;

    private String startTime;

    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(String startLocation) {
        this.startLocation = startLocation;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getLeftSeats() {
        return leftSeats;
    }

    public void setLeftSeats(String leftSeats) {
        this.leftSeats = leftSeats;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
}
