package org.iartisan.thought.web.integration.query;

import org.iartisan.runtime.api.utils.ApiResUtil;
import org.iartisan.runtime.bean.Page;
import org.iartisan.runtime.bean.PageWrapper;
import org.iartisan.runtime.env.EnvContextConfig;
import org.iartisan.runtime.api.base.BaseReq;
import org.iartisan.runtime.api.res.BaseListRes;
import org.iartisan.runtime.api.res.BaseOneRes;
import org.iartisan.runtime.api.res.BasePageRes;
import org.iartisan.runtime.exception.ApiRemoteException;
import org.iartisan.thought.server.api.query.TopicQueryFacade;
import org.iartisan.thought.server.api.req.BaseTopicReq;
import org.iartisan.thought.server.api.req.MyTopicQueryReq;
import org.iartisan.thought.server.api.req.TopicQueryReq;
import org.iartisan.thought.server.api.res.TopicBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 贴子服务
 *
 * @author King
 * @since 2017/12/11
 */
@Service
public class TopicQueryClient {

    @Autowired
    private TopicQueryFacade topicQueryFacade;

    public List<TopicBean> getTopTopicList() throws ApiRemoteException {
        BaseReq req = new BaseReq();
        req.setSystemCode(EnvContextConfig.get("app.name"));
        BaseListRes<TopicBean> res = topicQueryFacade.getTopTopicList(req);
        return ApiResUtil.getDataList(res);
    }

    public PageWrapper<TopicBean> getTopicPageData(TopicQueryReq req, Page page) throws ApiRemoteException {
        req.setSystemCode(EnvContextConfig.get("app.name"));
        BasePageRes<TopicBean> res = topicQueryFacade.getTopicPageData(req, page);
        return ApiResUtil.getDataPage(res);
    }

    public TopicBean getTopicDetailById(String id) throws ApiRemoteException {
        BaseTopicReq req = new BaseTopicReq();
        req.setSystemCode(EnvContextConfig.get("app.name"));
        req.setTopicId(id);
        BaseOneRes<TopicBean> res = topicQueryFacade.getTopicDetailById(req);
        return ApiResUtil.getBean(res);
    }

    public PageWrapper<TopicBean> getMyTopicPageData(Page page, String custId) throws ApiRemoteException {
        MyTopicQueryReq req = new MyTopicQueryReq();
        req.setSystemCode(EnvContextConfig.get("app.name"));
        req.setCustId(custId);
        BasePageRes<TopicBean> res = topicQueryFacade.getMyTopicPageData(req, page);
        return ApiResUtil.getDataPage(res);
    }

    public List<TopicBean> getHotTopicList() throws ApiRemoteException {
        BaseListRes<TopicBean> res = topicQueryFacade.getHotTopicList(new BaseReq());
        return ApiResUtil.getDataList(res);
    }
}
