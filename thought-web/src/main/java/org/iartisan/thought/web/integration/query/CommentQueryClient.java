package org.iartisan.thought.web.integration.query;

import org.iartisan.runtime.api.utils.ApiResUtil;
import org.iartisan.runtime.bean.Page;
import org.iartisan.runtime.bean.PageWrapper;
import org.iartisan.runtime.api.res.BasePageRes;
import org.iartisan.runtime.exception.ApiRemoteException;
import org.iartisan.runtime.utils.BeanUtil;
import org.iartisan.runtime.utils.StringUtils;
import org.iartisan.thought.server.api.query.CommentQueryFacade;
import org.iartisan.thought.server.api.query.ZanQueryFacade;
import org.iartisan.thought.server.api.req.BaseTopicReq;
import org.iartisan.thought.server.api.req.CommentZanReq;
import org.iartisan.thought.server.api.res.CommentBean;
import org.iartisan.thought.web.vo.CommentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * comment query client
 *
 * @author King
 * @since 2018/1/4
 */
@Service
public class CommentQueryClient {

    @Autowired
    private CommentQueryFacade commentQueryFacade;

    @Autowired
    private ZanQueryFacade zanQueryFacade;

    public PageWrapper<CommentVO> getCommentPageData(Page page, String topicId, String custId) throws ApiRemoteException {
        BaseTopicReq req = new BaseTopicReq();
        req.setTopicId(topicId);
        BasePageRes<CommentBean> res = commentQueryFacade.getCommentPageData(req, page);
        PageWrapper<CommentVO> result = BeanUtil.copyPage(ApiResUtil.getDataPage(res), CommentVO.class);
        //判断 这个comment 是否被登录人zan过
        if (StringUtils.isNotEmpty(custId)) {
            for (CommentVO commentVO : result.getData()) {
                CommentZanReq commentZanReq = new CommentZanReq();
                commentZanReq.setCommentId(commentVO.getId());
                commentZanReq.setCustId(custId);
                commentVO.setZaned(ApiResUtil.check(zanQueryFacade.isZaned(commentZanReq)));
            }
        }
        return result;
    }
}
