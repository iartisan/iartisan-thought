package org.iartisan.thought.web.controller.management;

import org.iartisan.runtime.exception.ApiRemoteException;
import org.iartisan.runtime.web.WebR;
import org.iartisan.runtime.web.contants.ReqContants;
import org.iartisan.runtime.web.controller.BaseController;
import org.iartisan.thought.server.api.req.CustActivateReq;
import org.iartisan.thought.server.api.req.CustCipAddReq;
import org.iartisan.thought.web.integration.management.CustManagementClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * req controller
 *
 * @author King
 * @since 2017/12/15
 */
@Controller
@RequestMapping("reg")
public class RegManagementController extends BaseController {

    @Autowired
    private CustManagementClient custManagementClient;

    @GetMapping(ReqContants.REQ_ADD_DATA_PAGE)
    public String addDataPage() {
        return "reg";
    }

    @ResponseBody
    @PostMapping(value = ReqContants.REQ_ADD_DATA)
    public WebR addData(CustCipAddReq req) throws ApiRemoteException {
        WebR r = new WebR();
        custManagementClient.addCustCip(req);
        r.setMessage("用户已注册，请登录邮件进行激活");
        return r;
    }

    @GetMapping("active")
    public String active(String custId, String token, Model model) {
        CustActivateReq req = new CustActivateReq();
        req.setCustId(custId);
        req.setToken(token);
        try {
            custManagementClient.active(req);
            model.addAttribute("result", true);
        } catch (ApiRemoteException e) {
            model.addAttribute("result", false);
        }
        return "/active";
    }
}
