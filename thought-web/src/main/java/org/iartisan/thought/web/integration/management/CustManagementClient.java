package org.iartisan.thought.web.integration.management;

import org.iartisan.runtime.api.utils.ApiResUtil;
import org.iartisan.runtime.env.EnvContextConfig;
import org.iartisan.runtime.exception.ApiRemoteException;
import org.iartisan.thought.server.api.management.CustManagementFacade;
import org.iartisan.thought.server.api.req.CustActivateReq;
import org.iartisan.thought.server.api.req.CustCipAddReq;
import org.iartisan.thought.server.api.req.CustModifyReq;
import org.iartisan.thought.server.api.req.CustRestPwdReq;
import org.iartisan.thought.server.api.res.CustAddRes;
import org.iartisan.thought.web.support.EmailSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * cust management client impl
 *
 * @author King
 * @since 2017/12/16
 */
@Service
public class CustManagementClient {

    @Autowired
    private CustManagementFacade custManagementFacade;

    @Autowired
    private EmailSupport emailSupport;

    public void addCustCip(CustCipAddReq req) throws ApiRemoteException {
        CustAddRes custAddRes = ApiResUtil.getBean(custManagementFacade.addCustData(req));
        //拼接验证链接
        StringBuffer urlBuffer = new StringBuffer(EnvContextConfig.get("active.email.host", "http://127.0.0.1:8080/reg/active"));
        urlBuffer.append("?custId=").append(custAddRes.getCustId());
        urlBuffer.append("&").append("token=").append(custAddRes.getToken());
        StringBuffer content = new StringBuffer("<html>");
        content.append("<body>").append("您好，欢迎注册我们网站，<a href='").append(urlBuffer.toString())
                .append("'>").append("请点击链接激活").append("</a>")
                .append("</body>").append("</html>");
        emailSupport.sendHtmlMail(req.getCustEmail(), EnvContextConfig.get("active.email.subject", "账号激活"), content.toString());
    }


    public void setAvatar(String custId, String avatar) {
        CustModifyReq req = new CustModifyReq();
        req.setCustId(custId);
        req.setAvatar(avatar);
        custManagementFacade.setCustAvatar(req);
    }

    public void modifyCustData(CustModifyReq req) throws ApiRemoteException {
        ApiResUtil.getResult(custManagementFacade.modifyCustData(req));
    }

    public void resetPwd(CustRestPwdReq req) throws ApiRemoteException {
        ApiResUtil.getResult(custManagementFacade.restPwd(req));
    }

    public void active(CustActivateReq req) throws ApiRemoteException {
        ApiResUtil.getResult(custManagementFacade.activate(req));
    }
}
