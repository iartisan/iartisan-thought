package org.iartisan.thought.server.dbm.model.extend;


import org.iartisan.thought.server.dbm.model.TopicDO;

import java.util.Date;

/**
 * <p>
 * topic User
 *
 * @author King
 * @since 2017/12/20
 */

public class TopicUserDO extends TopicDO {

    /**
     * 列名: AVATAR
     * 备注: 头像路径
     */

    private String avatar;

    /**
     * 列名: CUST_NICKNAME
     * 备注: 昵称
     */

    private String custNickname;

    /**
     * 列名: CATEGORY_KEY
     * 备注: 编码
     */

    private String categoryKey;

    /**
     * 列名: CATEGORY_NAME
     * 备注: 名称
     */

    private String categoryName;


    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCustNickname() {
        return custNickname;
    }

    public void setCustNickname(String custNickname) {
        this.custNickname = custNickname;
    }

    public String getCategoryKey() {
        return categoryKey;
    }

    public void setCategoryKey(String categoryKey) {
        this.categoryKey = categoryKey;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
