package org.iartisan.thought.server.dbm.mapper;


import org.apache.ibatis.annotations.CacheNamespace;
import org.iartisan.thought.server.dbm.model.BasicCategoryDO;
import org.iartisan.runtime.jdbc.MybatisBaseMapper;
import org.springframework.cache.annotation.Cacheable;

/**
 * basic_category 表操作接口
 *
 * @author King
 */
@CacheNamespace
public interface BasicCategoryMapper extends MybatisBaseMapper<BasicCategoryDO> {

}