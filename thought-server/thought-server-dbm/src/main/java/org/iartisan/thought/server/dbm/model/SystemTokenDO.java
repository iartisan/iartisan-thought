package org.iartisan.thought.server.dbm.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.util.Date;

/**
 * system_token 表模型
 *
 * @author King
 */
@TableName(value = "system_token")
public class SystemTokenDO {

    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;
    /**
     * 列名: USER_ID
     * 备注:
     */
    @TableField("USER_ID")
    private String userId;

    /**
     * 列名: TOKEN
     * 备注: token
     */
    @TableField("TOKEN")
    private String token;

    /**
     * 列名: TOKEN
     * 备注: 业务类型
     */
    @TableField("BUSINESS_TYPE")
    private String businessType;

    /**
     * 列名: EXPIRE_TIME
     * 备注: 过期时间
     */
    @TableField("EXPIRE_TIME")
    private Date expireTime;

    /**
     * 列名: UPDATE_TIME
     * 备注: 更新时间
     */
    @TableField("UPDATE_TIME")
    private Date updateTime;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}