package org.iartisan.thought.server.dbm.mapper;


import org.iartisan.thought.server.dbm.model.WarmChannelDO;
import org.iartisan.runtime.jdbc.MybatisBaseMapper;
/**
 * warm_channel 表操作接口
 * @author King
 */
public interface WarmChannelMapper extends MybatisBaseMapper<WarmChannelDO>{

}