package org.iartisan.thought.server.dbm.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.KeySequence;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import org.apache.ibatis.type.Alias;

/**
 * cust_topic_collect 表模型
 *
 * @author King
 */
@Alias("custTopicCollect")
@TableName(value = "cust_topic_collect")
public class CustTopicCollectDO implements Serializable {
    /**
     * 列名: ID
     * 备注: 自增主键
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    /**
     * 列名: CUSTOMER_ID
     * 备注: 发贴人ID
     */
    @TableField("CUSTOMER_ID")
    private String customerId;

    /**
     * 列名: TOPIC_ID
     * 备注: TOPIC_ID
     */
    @TableField("TOPIC_ID")
    private String topicId;

    /**
     * 列名: STATUS
     * 备注: 状态 E:有效 D:无效
     */
    @TableField("STATUS")
    private String status;

    /**
     * 列名: CREATE_TIME
     * 备注: 创建时间
     */
    @TableField("CREATE_TIME")
    private Date createTime;

    /**
     * 列名: UPDATE_TIME
     * 备注: 更新时间
     */
    @TableField("UPDATE_TIME")
    private Date updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}