package org.iartisan.thought.server.dbm.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.ibatis.type.Alias;

/**
 * topic 表模型
 *
 * @author King
 */
@Alias("topic")
@TableName(value = "topic")
public class TopicDO implements Serializable {

    /**
     * 列名: ID
     * 备注: 自增主键
     */
    @TableId("ID")
    private String id;

    /**
     * 列名: CUSTOMER_ID
     * 备注: 发贴人ID
     */
    @TableField("CUSTOMER_ID")
    private String customerId;

    /**
     * 列名: TITLE
     * 备注: 标题
     */
    @TableField("TITLE")
    private String title;

    /**
     * 列名: CONTENT
     * 备注: 内容
     */
    @TableField("CONTENT")
    private String content;

    /**
     * 列名: CATEGORY_ID
     * 备注: 类别ID
     */
    @TableField("CATEGORY_KEY")
    private String categoryKey;

    /**
     * 列名: STATUS
     * 备注: 状态 00:有效 01:待审核 02:删除
     */
    @TableField("STATUS")
    private String status;

    /**
     * 列名: IS_TOP
     * 备注: 是否置顶 0:是 1:否
     */
    @TableField("IS_TOP")
    private String isTop;

    /**
     * 列名: IS_FINE
     * 备注: 是否精贴 0:是 1:否
     */
    @TableField("IS_FINE")
    private String isFine;

    /**
     * 列名: IS_END
     * 备注: 是否结束 0:是 1:否
     */
    @TableField("IS_END")
    private String isEnd;

    /**
     * 列名: COMMENT_COUNT
     * 备注: 评论数
     */
    @TableField("COMMENT_COUNT")
    private Integer commentCount;

    /**
     * 列名: READ_COUNT
     * 备注: 浏览数
     */
    @TableField("READ_COUNT")
    private Integer readCount;

    /**
     * 列名: CREATE_TIME
     * 备注: 创建时间
     */
    @TableField("CREATE_TIME")
    private Date createTime;

    /**
     * 列名: UPDATE_TIME
     * 备注: 更新时间
     */
    @TableField("UPDATE_TIME")
    private Date updateTime;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCategoryKey() {
        return categoryKey;
    }

    public void setCategoryKey(String categoryKey) {
        this.categoryKey = categoryKey;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsTop() {
        return isTop;
    }

    public void setIsTop(String isTop) {
        this.isTop = isTop;
    }

    public String getIsFine() {
        return isFine;
    }

    public void setIsFine(String isFine) {
        this.isFine = isFine;
    }

    public String getIsEnd() {
        return isEnd;
    }

    public void setIsEnd(String isEnd) {
        this.isEnd = isEnd;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public Integer getReadCount() {
        return readCount;
    }

    public void setReadCount(Integer readCount) {
        this.readCount = readCount;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}