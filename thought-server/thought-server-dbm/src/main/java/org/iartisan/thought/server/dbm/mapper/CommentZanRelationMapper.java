package org.iartisan.thought.server.dbm.mapper;


import org.iartisan.thought.server.dbm.model.CommentZanRelationDO;
import org.iartisan.runtime.jdbc.MybatisBaseMapper;
/**
 * comment_zan_relation 表操作接口
 * @author King
 */
public interface CommentZanRelationMapper extends MybatisBaseMapper<CommentZanRelationDO>{

}