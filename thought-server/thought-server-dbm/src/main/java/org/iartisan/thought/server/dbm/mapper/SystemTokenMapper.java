package org.iartisan.thought.server.dbm.mapper;


import org.iartisan.runtime.jdbc.MybatisBaseMapper;
import org.iartisan.thought.server.dbm.model.SystemTokenDO;

/**
 * system_token 表操作接口
 * @author King
 */
public interface SystemTokenMapper extends MybatisBaseMapper<SystemTokenDO> {

}