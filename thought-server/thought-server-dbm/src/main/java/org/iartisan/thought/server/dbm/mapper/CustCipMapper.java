package org.iartisan.thought.server.dbm.mapper;


import org.iartisan.thought.server.dbm.model.CustCipDO;
import org.iartisan.runtime.jdbc.MybatisBaseMapper;
/**
 * cust_cip 表操作接口
 * @author King
 */
public interface CustCipMapper extends MybatisBaseMapper<CustCipDO>{

}