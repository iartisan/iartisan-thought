package org.iartisan.thought.server.dbm.model;

    import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
    import com.baomidou.mybatisplus.enums.IdType;
    import org.apache.ibatis.type.Alias;
/**
* user_message 表模型
* @author King
*/
@Alias("userMessage")
@TableName(value = "user_message")
public class UserMessageDO {

        /**
        * 列名: ID
        * 备注: 自增键
        */
        @TableId(value = "ID", type = IdType.AUTO)
        private Integer id;

        /**
        * 列名: FROM_USER
        * 备注: 来源
        */
        @TableField("FROM_USER")
        private String fromUser;

        /**
        * 列名: TO_USER
        * 备注: 目标
        */
        @TableField("TO_USER")
        private String toUser;

        /**
        * 列名: SUBJECT
        * 备注: 主题
        */
        @TableField("SUBJECT")
        private String subject;

        /**
        * 列名: CONTENT
        * 备注: 内容
        */
        @TableField("CONTENT")
        private String content;

        /**
        * 列名: IS_READ
        * 备注: 已读、未读 状态
        */
        @TableField("IS_READ")
        private String isRead;

        /**
        * 列名: CREATE_TIME
        * 备注: 创建时间
        */
        @TableField("CREATE_TIME")
        private Date createTime;

        /**
        * 列名: UPDATE_TIME
        * 备注: 更新时间
        */
        @TableField("UPDATE_TIME")
        private Date updateTime;


        public Integer getId(){
        return id;
      }

        public void setId(Integer id){
          this.id = id;
      }

        public String getFromUser(){
        return fromUser;
      }

        public void setFromUser(String fromUser){
          this.fromUser = fromUser;
      }

        public String getToUser(){
        return toUser;
      }

        public void setToUser(String toUser){
          this.toUser = toUser;
      }

        public String getSubject(){
        return subject;
      }

        public void setSubject(String subject){
          this.subject = subject;
      }

        public String getContent(){
        return content;
      }

        public void setContent(String content){
          this.content = content;
      }

        public String getIsRead(){
        return isRead;
      }

        public void setIsRead(String isRead){
          this.isRead = isRead;
      }

        public Date getCreateTime(){
        return createTime;
      }

        public void setCreateTime(Date createTime){
          this.createTime = createTime;
      }

        public Date getUpdateTime(){
        return updateTime;
      }

        public void setUpdateTime(Date updateTime){
          this.updateTime = updateTime;
      }

}