package org.iartisan.thought.server.dbm.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.ibatis.type.Alias;

/**
 * basic_category 表模型
 *
 * @author King
 */
@Alias("basicCategory")
@TableName(value = "basic_category")
public class BasicCategoryDO implements Serializable {

    /**
     * 列名: ID
     * 备注: 自增主键
     */
    @TableId
    @TableField("ID")
    private Integer id;

    /**
     * 列名: CATEGORY_KEY
     * 备注: 编码
     */
    @TableField("CATEGORY_KEY")
    private String categoryKey;

    /**
     * 列名: CATEGORY_NAME
     * 备注: 名称
     */
    @TableField("CATEGORY_NAME")
    private String categoryName;

    /**
     * 列名: CREATE_TIME
     * 备注: 创建时间
     */
    @TableField("CREATE_TIME")
    private Date createTime;

    /**
     * 列名: UPDATE_TIME
     * 备注: 更新时间
     */
    @TableField("UPDATE_TIME")
    private Date updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategoryKey() {
        return categoryKey;
    }

    public void setCategoryKey(String categoryKey) {
        this.categoryKey = categoryKey;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}