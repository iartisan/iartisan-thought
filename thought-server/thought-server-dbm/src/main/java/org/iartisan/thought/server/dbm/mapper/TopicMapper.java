package org.iartisan.thought.server.dbm.mapper;


import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.session.RowBounds;
import org.iartisan.thought.server.dbm.model.TopicDO;
import org.iartisan.runtime.jdbc.MybatisBaseMapper;
import org.iartisan.thought.server.dbm.model.extend.TopicUserDO;
import java.util.List;

/**
 * topic 表操作接口
 *
 * @author King
 */
public interface TopicMapper extends MybatisBaseMapper<TopicDO> {

    List<TopicUserDO> selectTopicUserData(TopicDO topicDO);

    List<TopicUserDO> selectTopicUserData(RowBounds rowBounds, TopicUserDO wrapper);

}