package org.iartisan.thought.server.api.req;

import org.iartisan.runtime.api.base.BaseReq;
/**
 * <p>
 * 贴子修改req
 *
 * @author King
 * @since 2018/5/15
 */
public class TopicModifyReq extends BaseReq {

    private String topicId;

    private String content;

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
