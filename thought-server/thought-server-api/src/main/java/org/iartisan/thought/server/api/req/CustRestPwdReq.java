package org.iartisan.thought.server.api.req;

/**
 * <p>
 * 重置个人密码
 *
 * @author King
 * @since 2018/5/18
 */
public class CustRestPwdReq extends BaseCustReq {

    //老password
    private String oldPwd;

    //新password
    private String newPwd;

    public String getOldPwd() {
        return oldPwd;
    }

    public void setOldPwd(String oldPwd) {
        this.oldPwd = oldPwd;
    }

    public String getNewPwd() {
        return newPwd;
    }

    public void setNewPwd(String newPwd) {
        this.newPwd = newPwd;
    }
}
