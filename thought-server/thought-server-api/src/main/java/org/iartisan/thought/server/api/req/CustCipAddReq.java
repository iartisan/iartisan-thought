package org.iartisan.thought.server.api.req;

import java.io.Serializable;

/**
 * <p>
 * cip add req
 *
 * @author King
 * @since 2017/12/16
 */
public class CustCipAddReq implements Serializable {


    private String custEmail;//邮箱 默认为账号

    private String custPwd;//密码

    private String custUserName;//用户名

    public String getCustEmail() {
        return custEmail;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    public String getCustPwd() {
        return custPwd;
    }

    public void setCustPwd(String custPwd) {
        this.custPwd = custPwd;
    }

    public String getCustUserName() {
        return custUserName;
    }

    public void setCustUserName(String custUserName) {
        this.custUserName = custUserName;
    }
}
