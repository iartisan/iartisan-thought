package org.iartisan.thought.server.api.req;

/**
 * @author King
 * @since 2018/1/16
 */
public class CustModifyReq extends BaseCustReq {

    private String avatar;

    //性别
    private String custGender;

    //昵称
    private String custNickName;

    //签名
    private String sign;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCustGender() {
        return custGender;
    }

    public void setCustGender(String custGender) {
        this.custGender = custGender;
    }

    public String getCustNickName() {
        return custNickName;
    }

    public void setCustNickName(String custNickName) {
        this.custNickName = custNickName;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
