package org.iartisan.thought.server.api.res;

import java.io.Serializable;

/**
 * <p>
 * category bean
 *
 * @author King
 * @since 2017/12/19
 */
public class CategoryBean implements Serializable {

    private String categoryKey;

    private String categoryName;

    public String getCategoryKey() {
        return categoryKey;
    }

    public void setCategoryKey(String categoryKey) {
        this.categoryKey = categoryKey;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
