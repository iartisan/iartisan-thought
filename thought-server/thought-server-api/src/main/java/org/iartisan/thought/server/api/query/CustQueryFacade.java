package org.iartisan.thought.server.api.query;

import org.iartisan.runtime.api.res.BaseOneRes;
import org.iartisan.thought.server.api.req.BaseCustReq;
import org.iartisan.thought.server.api.req.CustLoginReq;
import org.iartisan.thought.server.api.res.CustBean;

/**
 * <p>
 * cust query
 *
 * @author King
 * @since 2017/12/15
 */
public interface CustQueryFacade {

    /**
     * 会员登录
     *
     * @return
     */
    BaseOneRes<CustBean> custLogin(CustLoginReq req);

    /**
     * 管理员登录
     *
     * @return
     */
    BaseOneRes<CustBean> adminLogin();

    BaseOneRes<CustBean> getCustByCustId(BaseCustReq req);

}
