package org.iartisan.thought.server.api.query;

import org.iartisan.runtime.api.res.BaseCheckRes;
import org.iartisan.runtime.api.res.BasePageRes;
import org.iartisan.runtime.bean.Page;
import org.iartisan.thought.server.api.req.TopicCollectReq;
import org.iartisan.thought.server.api.res.TopicBean;

/**
 * <p>
 * 贴子查询服务
 *
 * @author King
 * @since 2017/12/8
 */
public interface CustTopicCollectQueryFacade {

    //判断贴子是否被收藏
    BaseCheckRes checkTopicCollect(TopicCollectReq req);


    /**
     * 查询我收藏的贴
     *
     * @param req
     * @param page
     * @return
     */
    BasePageRes<TopicBean> getMyCollectTopicPageData(TopicCollectReq req, Page page);
}
