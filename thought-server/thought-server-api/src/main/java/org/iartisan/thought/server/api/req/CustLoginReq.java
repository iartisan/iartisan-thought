package org.iartisan.thought.server.api.req;

import org.iartisan.runtime.api.base.BaseReq;

/**
 * <p>
 * cust login req
 *
 * @author King
 * @since 2017/12/15
 */
public class CustLoginReq extends BaseReq {

    private String custAccount;

    private String custPwd;

    public String getCustAccount() {
        return custAccount;
    }

    public void setCustAccount(String custAccount) {
        this.custAccount = custAccount;
    }


    public String getCustPwd() {
        return custPwd;
    }

    public void setCustPwd(String custPwd) {
        this.custPwd = custPwd;
    }
}
