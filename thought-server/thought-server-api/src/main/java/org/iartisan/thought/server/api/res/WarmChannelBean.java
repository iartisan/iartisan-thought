package org.iartisan.thought.server.api.res;

import java.io.Serializable;

/**
 * @author King
 * @since 2018/1/30
 */
public class WarmChannelBean implements Serializable {

    private String channelType;

    private String channelLocation;

    private String channelName;

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public String getChannelLocation() {
        return channelLocation;
    }

    public void setChannelLocation(String channelLocation) {
        this.channelLocation = channelLocation;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }
}
