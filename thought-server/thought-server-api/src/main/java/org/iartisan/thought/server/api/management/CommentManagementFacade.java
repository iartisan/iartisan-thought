package org.iartisan.thought.server.api.management;


import org.iartisan.runtime.api.base.BaseRes;
import org.iartisan.thought.server.api.req.BaseCommentReq;
import org.iartisan.thought.server.api.req.CommentAddReq;
import org.iartisan.thought.server.api.req.CommentZanReq;

/**
 * <p>
 * comment management
 *
 * @author King
 * @since 2017/12/15
 */
public interface CommentManagementFacade {

    /**
     * 添加会员 数据
     *
     * @param req
     * @return
     */
    BaseRes addCommentData(CommentAddReq req);

    /**
     * 采纳操作
     *
     * @param req
     * @return
     */
    BaseRes accept(BaseCommentReq req);

    /**
     * zan
     *
     * @param req
     * @return
     */
    BaseRes addZan(CommentZanReq req);

}
