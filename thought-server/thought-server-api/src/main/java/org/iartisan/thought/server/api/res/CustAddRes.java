package org.iartisan.thought.server.api.res;

import java.io.Serializable;

/**
 * <p>
 * 会员信息添加返回
 *
 * @author King
 * @since 2018/5/25
 */
public class CustAddRes implements Serializable {

    private String custId;//会员id

    private String token;//邮箱验证token

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
