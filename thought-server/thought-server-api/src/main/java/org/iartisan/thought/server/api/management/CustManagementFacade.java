package org.iartisan.thought.server.api.management;


import org.iartisan.runtime.api.base.BaseRes;
import org.iartisan.runtime.api.res.BaseOneRes;
import org.iartisan.thought.server.api.req.CustActivateReq;
import org.iartisan.thought.server.api.req.CustCipAddReq;
import org.iartisan.thought.server.api.req.CustModifyReq;
import org.iartisan.thought.server.api.req.CustRestPwdReq;
import org.iartisan.thought.server.api.res.CustAddRes;

/**
 * <p>
 * cust query
 *
 * @author King
 * @since 2017/12/15
 */
public interface CustManagementFacade {

    /**
     * 添加会员 数据
     *
     * @param req
     * @return
     */
    BaseOneRes<CustAddRes> addCustData(CustCipAddReq req);

    /**
     * 设置头像
     *
     * @param req
     * @return
     */
    BaseRes setCustAvatar(CustModifyReq req);

    /**
     * 修改个人信息
     *
     * @param req
     * @return
     */
    BaseRes modifyCustData(CustModifyReq req);

    /**
     * 重置个人密码
     *
     * @return
     */
    BaseRes restPwd(CustRestPwdReq req);

    /**
     * 用户激活
     *
     * @param req
     * @return
     */
    BaseRes activate(CustActivateReq req);

}
