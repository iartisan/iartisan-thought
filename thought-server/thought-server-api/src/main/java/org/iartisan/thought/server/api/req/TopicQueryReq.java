package org.iartisan.thought.server.api.req;

import org.iartisan.runtime.api.base.BaseReq;

/**
 * <p>
 * 添加查询
 *
 * @author King
 * @since 2017/12/14
 */
public class TopicQueryReq extends BaseReq {

   private String isEnd;

   private String isTop;

   private String categoryKey;

    public String getIsEnd() {
        return isEnd;
    }

    public void setIsEnd(String isEnd) {
        this.isEnd = isEnd;
    }

    public String getCategoryKey() {
        return categoryKey;
    }

    public void setCategoryKey(String categoryKey) {
        this.categoryKey = categoryKey;
    }

    public String getIsTop() {
        return isTop;
    }

    public void setIsTop(String isTop) {
        this.isTop = isTop;
    }
}
