package org.iartisan.thought.server.api.req;

import org.iartisan.runtime.api.base.BaseReq;

/**
 * <p>
 * 添加贴子
 *
 * @author King
 * @since 2017/12/14
 */
public class TopicAddReq extends BaseReq {

    private String custId;

    private String title;

    private String content;

    private String categoryKey;

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCategoryKey() {
        return categoryKey;
    }

    public void setCategoryKey(String categoryKey) {
        this.categoryKey = categoryKey;
    }
}
