package org.iartisan.thought.server.api.req;

import org.iartisan.runtime.api.base.BaseReq;

/**
 * <p>
 * zan
 *
 * @author King
 * @since 2018/2/7
 */
public class CommentZanReq extends BaseReq {

    private String commentId;

    private String custId;

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }
}
