package org.iartisan.thought.server.api.query;

import org.iartisan.runtime.api.base.BaseReq;
import org.iartisan.runtime.api.base.BaseRes;
import org.iartisan.runtime.api.res.BaseListRes;
import org.iartisan.thought.server.api.req.BaseTopicReq;
import org.iartisan.thought.server.api.res.WarmChannelBean;

/**
 * <p>
 * 温馨通道查询
 *
 * @author King
 * @since 2018/1/30
 */
public interface WarmChannelQueryFacade {

    /**
     * 添加温馨通道
     *
     * @param req
     * @return
     */
    BaseListRes<WarmChannelBean> getWarmChannels(BaseReq req);
}
