package org.iartisan.thought.server.api.req;

import org.iartisan.runtime.api.base.BaseReq;

/**
 * <p>
 * base topic req
 *
 * @author King
 * @since 2017/12/19
 */
public class BaseTopicReq extends BaseReq {

    private String topicId;

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }
}
