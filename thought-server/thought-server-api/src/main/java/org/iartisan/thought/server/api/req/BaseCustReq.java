package org.iartisan.thought.server.api.req;

import org.iartisan.runtime.api.base.BaseReq;

/**
 * <p>
 * base cust req
 *
 * @author King
 * @since 2017/12/19
 */
public class BaseCustReq extends BaseReq {

    private String custId;

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(super.toString()).append("BaseCustReq{");
        sb.append("custId='").append(custId).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
