package org.iartisan.thought.server.api.management;

import org.iartisan.runtime.api.base.BaseRes;
import org.iartisan.thought.server.api.req.BaseCustReq;
import org.iartisan.thought.server.api.req.BaseIdReq;

/**
 * <p>
 * 消息 management
 *
 * @author King
 * @since 2018/2/6
 */
public interface UserMessageManagementFacade {

    /**
     * 删除消息
     *
     * @param req
     * @return
     */
    BaseRes deleteByCustId(BaseCustReq req);

    /**
     * 设置已读
     *
     * @param req
     * @return
     */
    BaseRes setReadByCustId(BaseCustReq req);

    /**
     * 设置已读
     *
     * @param req
     * @return
     */
    BaseRes setReadById(BaseIdReq req);

    /**
     * 删除消息
     *
     * @param req
     * @return
     */
    BaseRes deleteById(BaseIdReq req);

}
