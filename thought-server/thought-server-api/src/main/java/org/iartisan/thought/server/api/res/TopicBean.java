package org.iartisan.thought.server.api.res;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * topic bean
 *
 * @author King
 * @since 2017/12/8
 */
public class TopicBean implements Serializable {

    private String id;

    private String topicId;

    private String customerId;

    private String title;

    private String content;

    private String categoryId;

    private Integer commentCount;

    private Integer readCount;

    private String status;

    private String isTop;

    private String isFine;

    private String isEnd;

    private Date createTime;

    private Date updateTime;

    /**
     * 列名: AVATAR
     * 备注: 头像路径
     */

    private String avatar;

    /**
     * 列名: CUST_NICKNAME
     * 备注: 昵称
     */

    private String custNickname;

    /**
     * 列名: CATEGORY_KEY
     * 备注: 编码
     */

    private String categoryKey;

    /**
     * 列名: CATEGORY_NAME
     * 备注: 名称
     */

    private String categoryName;


    private String canAccept;//回复是否允许采纳

    private String isWarmChannel;//是否温馨通道

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public Integer getReadCount() {
        return readCount;
    }

    public void setReadCount(Integer readCount) {
        this.readCount = readCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsTop() {
        return isTop;
    }

    public void setIsTop(String isTop) {
        this.isTop = isTop;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getIsFine() {
        return isFine;
    }

    public void setIsFine(String isFine) {
        this.isFine = isFine;
    }

    public String getIsEnd() {
        return isEnd;
    }

    public void setIsEnd(String isEnd) {
        this.isEnd = isEnd;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCustNickname() {
        return custNickname;
    }

    public void setCustNickname(String custNickname) {
        this.custNickname = custNickname;
    }

    public String getCategoryKey() {
        return categoryKey;
    }

    public void setCategoryKey(String categoryKey) {
        this.categoryKey = categoryKey;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCanAccept() {
        return canAccept;
    }

    public void setCanAccept(String canAccept) {
        this.canAccept = canAccept;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getIsWarmChannel() {
        return isWarmChannel;
    }

    public void setIsWarmChannel(String isWarmChannel) {
        this.isWarmChannel = isWarmChannel;
    }
}
