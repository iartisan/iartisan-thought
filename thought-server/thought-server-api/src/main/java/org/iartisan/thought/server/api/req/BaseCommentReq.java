package org.iartisan.thought.server.api.req;

import java.io.Serializable;

/**
 * <p>
 * base comment req
 *
 * @author King
 * @since 2018/1/5
 */
public class BaseCommentReq implements Serializable {

    private String commentId;

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }
}
