package org.iartisan.thought.server.api.req;

import org.iartisan.runtime.api.base.BaseReq;

import java.io.Serializable;

/**
 * 适用于各种 id 请求
 *
 * @author King
 * @since 2018/2/7
 */
public class BaseIdReq extends BaseReq {

    private Serializable id;

    public Serializable getId() {
        return id;
    }

    public void setId(Serializable id) {
        this.id = id;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BaseIdReq{");
        sb.append("id=").append(id);
        sb.append('}');
        return sb.toString();
    }
}
