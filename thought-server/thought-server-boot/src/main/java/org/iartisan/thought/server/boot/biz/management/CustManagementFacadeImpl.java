package org.iartisan.thought.server.boot.biz.management;

import org.iartisan.runtime.api.base.BaseRes;
import org.iartisan.runtime.api.res.BaseOneRes;
import org.iartisan.runtime.exception.NotAllowedException;
import org.iartisan.thought.server.api.management.CustManagementFacade;
import org.iartisan.thought.server.api.req.CustActivateReq;
import org.iartisan.thought.server.api.req.CustCipAddReq;
import org.iartisan.thought.server.api.req.CustModifyReq;
import org.iartisan.thought.server.api.req.CustRestPwdReq;
import org.iartisan.thought.server.api.res.CustAddRes;
import org.iartisan.thought.server.service.management.CustManagementService;
import org.iartisan.thought.server.service.support.MessageHandler;
import org.iartisan.thought.server.service.support.TokenSupportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * cust management facade impl
 *
 * @author King
 * @since 2017/12/16
 */
@Service("custManagementFacade")
public class CustManagementFacadeImpl implements CustManagementFacade {

    @Autowired
    private CustManagementService custManagementService;

    @Autowired
    private TokenSupportService tokenSupportService;

    @Override
    @Transactional
    public BaseOneRes<CustAddRes> addCustData(CustCipAddReq req) {
        BaseOneRes<CustAddRes> res = new BaseOneRes();
        String custId = custManagementService.addCustCipData(req.getCustEmail(), req.getCustUserName(), req.getCustPwd());
        //创建激活token
        String token = tokenSupportService.createEmailActiveToken(custId);
        CustAddRes custAddRes = new CustAddRes();
        custAddRes.setCustId(custId);
        custAddRes.setToken(token);
        res.setDataObject(custAddRes);
        return res;
    }

    @Override
    public BaseRes setCustAvatar(CustModifyReq req) {
        BaseRes res = new BaseRes();
        custManagementService.modifyAvatar(req.getCustId(), req.getAvatar());
        return res;
    }

    @Override
    public BaseRes modifyCustData(CustModifyReq req) {
        BaseRes res = new BaseRes();
        custManagementService.modifyData(req.getCustId(), req.getCustNickName(), req.getCustGender(), req.getSign());
        return res;
    }

    @Override
    public BaseRes restPwd(CustRestPwdReq req) {
        BaseRes res = new BaseRes();
        custManagementService.resetPwd(req.getCustId(), req.getOldPwd(), req.getNewPwd());
        return res;
    }


    @Autowired
    private MessageHandler messageHandler;

    @Override
    public BaseRes activate(CustActivateReq req) {
        BaseRes res = new BaseRes();
        //判断token 是否可用
        if (tokenSupportService.checkToken(req.getToken())) {
            //设置用户状态
            custManagementService.enableCust(req.getCustId());
            //发送欢迎消息
            messageHandler.sendWelcomeMsg(req.getCustId());
            //
            tokenSupportService.deleteToken(req.getToken());
        } else {
            res.setNotAllowedErrorRes("token无效");
        }
        return res;
    }
}
