package org.iartisan.thought.server.boot.biz.query;

import org.iartisan.runtime.api.res.BaseCheckRes;
import org.iartisan.thought.server.api.query.ZanQueryFacade;
import org.iartisan.thought.server.api.req.CommentZanReq;
import org.iartisan.thought.server.service.query.CommentZanRelationQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author King
 * @since 2018/2/8
 */
@Service("zanQueryFacade")
public class ZanQueryFacadeImpl implements ZanQueryFacade {

    @Autowired
    private CommentZanRelationQueryService commentZanRelationQueryService;

    @Override
    public BaseCheckRes isZaned(CommentZanReq req) {
        BaseCheckRes res = new BaseCheckRes();
        boolean result = commentZanRelationQueryService.isZan(req.getCommentId(), req.getCustId());
        res.setResult(result);
        return res;
    }
}
