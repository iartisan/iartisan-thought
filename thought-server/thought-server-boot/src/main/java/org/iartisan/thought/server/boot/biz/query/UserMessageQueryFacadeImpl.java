package org.iartisan.thought.server.boot.biz.query;

import org.iartisan.runtime.api.res.BaseListRes;
import org.iartisan.runtime.api.res.BaseOneRes;
import org.iartisan.runtime.constants.FlagType;
import org.iartisan.runtime.utils.BeanUtil;
import org.iartisan.runtime.utils.CollectionUtil;
import org.iartisan.thought.server.api.query.UserMessageQueryFacade;
import org.iartisan.thought.server.api.req.BaseCustReq;
import org.iartisan.thought.server.api.res.UserMessageBean;
import org.iartisan.thought.server.service.bo.UserMessageBO;
import org.iartisan.thought.server.service.query.UserMessageQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author King
 * @since 2018/2/6
 */
@Service("userMessageQueryFacade")
public class UserMessageQueryFacadeImpl implements UserMessageQueryFacade {

    @Autowired
    private UserMessageQueryService userMessageQueryService;

    @Override
    public BaseOneRes<Integer> getUnreadMsgCount(BaseCustReq req) {
        BaseOneRes<Integer> res = new BaseOneRes<>();
        int count = userMessageQueryService.getUnreadMsgCount(req.getCustId());
        res.setDataObject(count);
        return res;
    }

    @Override
    public BaseListRes<UserMessageBean> getUnreadMsgs(BaseCustReq req) {
        BaseListRes<UserMessageBean> res = new BaseListRes<>();
        UserMessageBO messageBO = new UserMessageBO();
        messageBO.setToUser(req.getCustId());
        messageBO.setIsRead(FlagType.N.name());
        List<UserMessageBO> result = userMessageQueryService.getListDataByObjs(messageBO);
        if (CollectionUtil.isNotEmpty(result)) {
            List<UserMessageBean> dataList = new ArrayList<>();
            for (UserMessageBO o : result) {
                UserMessageBean bean = new UserMessageBean();
                bean.setId(o.getId());
                bean.setContent(o.getContent());
                bean.setCreateTime(o.getCreateTime());
                dataList.add(bean);
            }
            res.setDataList(dataList);
        }
        return res;
    }
}
