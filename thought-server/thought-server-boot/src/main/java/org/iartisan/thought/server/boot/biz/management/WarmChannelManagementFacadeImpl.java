package org.iartisan.thought.server.boot.biz.management;

import org.iartisan.runtime.api.base.BaseRes;
import org.iartisan.thought.server.api.management.WarmChannelManagementFacade;
import org.iartisan.thought.server.api.req.BaseTopicReq;
import org.iartisan.thought.server.service.management.WarmChannelManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author King
 * @since 2018/1/30
 */
@Service("warmChannelManagementFacade")
public class WarmChannelManagementFacadeImpl implements WarmChannelManagementFacade {

    @Autowired
    private WarmChannelManagementService warmChannelManagementService;

    @Override
    public BaseRes setWarmChannel(BaseTopicReq req) {
        BaseRes res = new BaseRes();
        warmChannelManagementService.setWarmChannel(req.getTopicId());
        return res;
    }

}
