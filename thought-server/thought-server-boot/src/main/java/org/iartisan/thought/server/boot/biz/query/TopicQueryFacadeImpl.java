package org.iartisan.thought.server.boot.biz.query;

import org.iartisan.runtime.bean.Page;
import org.iartisan.runtime.bean.PageWrapper;
import org.iartisan.runtime.api.base.BaseReq;
import org.iartisan.runtime.api.res.BaseListRes;
import org.iartisan.runtime.api.res.BaseOneRes;
import org.iartisan.runtime.api.res.BasePageRes;
import org.iartisan.runtime.constants.FlagType;
import org.iartisan.runtime.utils.BeanUtil;
import org.iartisan.thought.server.api.query.TopicQueryFacade;
import org.iartisan.thought.server.api.req.BaseTopicReq;
import org.iartisan.thought.server.api.req.MyTopicQueryReq;
import org.iartisan.thought.server.api.req.TopicQueryReq;
import org.iartisan.thought.server.api.res.TopicBean;
import org.iartisan.thought.server.service.bo.extend.TopicUserBO;
import org.iartisan.thought.server.service.query.CommentQueryService;
import org.iartisan.thought.server.service.query.TopicQueryService;
import org.iartisan.thought.server.service.bo.TopicBO;
import org.iartisan.thought.server.service.query.WarmChannelQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * implements
 *
 * @author King
 * @since 2017/12/8
 */
@Service("topicQueryFacade")
public class TopicQueryFacadeImpl implements TopicQueryFacade {

    @Autowired
    private TopicQueryService topicQueryService;

    @Autowired
    private CommentQueryService commentQueryService;

    @Autowired
    private WarmChannelQueryService warmChannelQueryService;

    @Override
    public BasePageRes<TopicBean> getMyTopicPageData(MyTopicQueryReq req, Page page) {
        BasePageRes<TopicBean> res = new BasePageRes<>();
        TopicBO topicBO = new TopicBO();
        topicBO.setCustomerId(req.getCustId());
        PageWrapper<TopicBO> dbPageWrapper = topicQueryService.getPageData(page, topicBO);
        res.setDataPage(BeanUtil.copyPage(dbPageWrapper, TopicBean.class));
        return res;
    }

    @Override
    public BasePageRes<TopicBean> getTopicPageData(TopicQueryReq req, Page page) {
        BasePageRes<TopicBean> res = new BasePageRes<>();
        TopicBO topicBO = new TopicBO();
        topicBO.setIsTop(req.getIsTop());
        topicBO.setIsEnd(req.getIsEnd());
        topicBO.setCategoryKey(req.getCategoryKey());
        PageWrapper<TopicUserBO> dbPageWrapper = topicQueryService.getTopicUserPageData(page, topicBO);
        res.setDataPage(BeanUtil.copyPage(dbPageWrapper, TopicBean.class));
        return res;
    }

    @Override
    public BaseListRes<TopicBean> getTopTopicList(BaseReq req) {
        TopicBO topicBO = new TopicBO();
        topicBO.setIsTop(FlagType.Y.toString());
        List<TopicUserBO> resultList = topicQueryService.getTopicUserList(topicBO);
        List<TopicBean> dataList = BeanUtil.copyList(resultList, TopicBean.class);
        BaseListRes<TopicBean> res = new BaseListRes<>();
        res.setDataList(dataList);
        return res;
    }

    @Override
    public BaseOneRes<TopicBean> getTopicDetailById(BaseTopicReq req) {
        TopicUserBO topicBO = topicQueryService.getTopicUserById(req.getTopicId());
        TopicBean bean = BeanUtil.copyBean(topicBO, TopicBean.class);
        bean.setCanAccept(commentQueryService.canAccept(req.getTopicId()));
        //是否温馨通道
        bean.setIsWarmChannel(warmChannelQueryService.isWarmChannelByTopicId(req.getTopicId()));
        BaseOneRes<TopicBean> res = new BaseOneRes<>();
        res.setDataObject(bean);
        return res;
    }

    @Override
    public BaseListRes<TopicBean> getHotTopicList(BaseReq req) {
        BaseListRes<TopicBean> res = new BaseListRes<>();
        List<TopicBO> result = topicQueryService.getHotTopicList();
        List<TopicBean> dataList = new ArrayList<>();
        if (null != result && result.size() > 0) {
            for (TopicBO topicBO : result) {
                TopicBean topicBean = new TopicBean();
                topicBean.setId(topicBO.getId());
                topicBean.setTitle(topicBO.getTitle());
                topicBean.setCommentCount(topicBO.getCommentCount());
                dataList.add(topicBean);
            }
        }
        res.setDataList(dataList);
        return res;
    }
}
