package org.iartisan.thought.server.boot.biz.query;

import org.iartisan.runtime.api.res.BaseOneRes;
import org.iartisan.thought.server.api.query.CustQueryFacade;
import org.iartisan.thought.server.api.req.BaseCustReq;
import org.iartisan.thought.server.api.req.CustLoginReq;
import org.iartisan.thought.server.api.res.CustBean;
import org.iartisan.thought.server.service.bo.CustBO;
import org.iartisan.thought.server.service.query.CustQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * cust query facade impl
 *
 * @author King
 * @since 2017/12/15
 */
@Service("custQueryFacade")
public class CustQueryFacadeImpl implements CustQueryFacade {

    @Autowired
    private CustQueryService custQueryService;

    @Override
    public BaseOneRes<CustBean> custLogin(CustLoginReq req) {
        BaseOneRes<CustBean> res = new BaseOneRes<>();
        CustBO custBO = custQueryService.getCustCipInfo(req.getCustAccount(), req.getCustPwd());
        if (null != custBO) {
            CustBean custBean = new CustBean();
            custBean.setCustId(custBO.getCustId());
            custBean.setCustUserName(custBO.getCustUserName());
            custBean.setCustType(custBO.getCustType());
            custBean.setAvatar(custBO.getAvatar());
            custBean.setStatus(custBO.getStatus());
            res.setDataObject(custBean);
        } else {
            res.setNotAllowedErrorRes("用户名或密码错误");
        }
        return res;
    }

    @Override
    public BaseOneRes<CustBean> adminLogin() {
        return null;
    }

    @Override
    public BaseOneRes<CustBean> getCustByCustId(BaseCustReq req) {
        BaseOneRes<CustBean> res = new BaseOneRes<>();
        CustBO custBO = custQueryService.getCustInfoById(req.getCustId());
        CustBean custBean = new CustBean();
        custBean.setAvatar(custBO.getAvatar());
        custBean.setCustId(custBO.getCustId());
        custBean.setCustUserName(custBO.getCustUserName());
        custBean.setCustGender(custBO.getCustGender());
        custBean.setCustAccount(custBO.getCustAccount());
        custBean.setSign(custQueryService.getSign(custBO.getCustId()));
        res.setDataObject(custBean);
        return res;
    }
}
