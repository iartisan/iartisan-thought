CREATE TABLE `cust_base` (
  `CUST_ID` varchar(48) NOT NULL COMMENT 'cust_id',
  `CUST_TYPE` varchar(10) DEFAULT NULL COMMENT '会员类型',
  `CUST_ACCOUNT` varchar(48) NOT NULL COMMENT '登录账号',
  `CUST_PWD` varchar(48) NOT NULL COMMENT '密码',
  `CUST_STATUS` varchar(1) DEFAULT 'E' COMMENT '状态 E:有效 D:删除',
  `AVATAR` varchar(200) DEFAULT NULL COMMENT '头像路径',
  `CUST_NICKNAME` varchar(200) DEFAULT NULL COMMENT '昵称',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '修改时间',
  `CUST_GENDER` varchar(1) DEFAULT NULL COMMENT '性别 F:女 M:男',
  PRIMARY KEY (`CUST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户基表';

ALTER TABLE cust_base add COLUMN CUST_USERNAME VARCHAR(30) UNIQUE  COMMENT '用户名称';
insert into cust_base (CUST_ID,CUST_TYPE,CUST_ACCOUNT,CUST_PWD) values('1','admin','wangkechaodream@163.com','e10adc3949ba59abbe56e057f20f883e');