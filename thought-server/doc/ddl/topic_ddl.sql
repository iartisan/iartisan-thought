CREATE TABLE `topic` (
  `ID` varchar(48) NOT NULL COMMENT '自增主键',
  `CUSTOMER_ID` varchar(48) NOT NULL COMMENT '发贴人ID',
  `TITLE` varchar(200) NOT NULL COMMENT '标题',
  `CONTENT` text NOT NULL COMMENT '内容',
  `CATEGORY_KEY` varchar(50) DEFAULT NULL,
  `STATUS` varchar(2) DEFAULT NULL COMMENT '状态 00:有效 01:待审核 02:删除',
  `IS_TOP` varchar(1) DEFAULT '1' COMMENT '是否置顶 0:是 1:否',
  `IS_FINE` varchar(1) DEFAULT '1' COMMENT '是否精贴 0:是 1:否',
  `IS_END` varchar(1) DEFAULT '1' COMMENT '是否结束 0:是 1:否',
  `COMMENT_COUNT` int(11) DEFAULT '0' COMMENT '评论数',
  `READ_COUNT` int(11) DEFAULT '0' COMMENT '浏览数',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='贴子';