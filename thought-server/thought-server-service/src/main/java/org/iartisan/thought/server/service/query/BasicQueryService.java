package org.iartisan.thought.server.service.query;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.iartisan.runtime.utils.BeanUtil;
import org.iartisan.thought.server.dbm.mapper.BasicCategoryMapper;
import org.iartisan.thought.server.dbm.model.BasicCategoryDO;
import org.iartisan.thought.server.service.bo.CategoryBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * basic query service
 *
 * @author King
 * @since 2017/12/19
 */
@Service
public class BasicQueryService {

    @Autowired
    private BasicCategoryMapper basicCategoryMapper;

    public List<CategoryBO> getCategories() {
        List<BasicCategoryDO> dbResult = basicCategoryMapper.selectList(new EntityWrapper<>());
        return BeanUtil.copyList(dbResult, CategoryBO.class);
    }
}
