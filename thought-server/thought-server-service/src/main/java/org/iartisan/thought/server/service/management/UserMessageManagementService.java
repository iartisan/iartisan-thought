package org.iartisan.thought.server.service.management;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import org.iartisan.runtime.constants.FlagType;
import org.iartisan.runtime.support.BaseManagementServiceSupport;
import org.iartisan.thought.server.dbm.mapper.UserMessageMapper;
import org.iartisan.thought.server.dbm.model.UserMessageDO;
import org.iartisan.thought.server.service.bo.UserMessageBO;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author King
 * @since 2018/2/1
 */

@Service
public class UserMessageManagementService extends BaseManagementServiceSupport<UserMessageMapper, UserMessageBO> {

    public void saveData(UserMessageBO userMessageBO) {
        UserMessageDO dbInsert = new UserMessageDO();
        dbInsert.setFromUser(userMessageBO.getFromUser());
        dbInsert.setToUser(userMessageBO.getToUser());
        dbInsert.setSubject(userMessageBO.getSubject());
        dbInsert.setContent(userMessageBO.getContent());
        dbInsert.setCreateTime(new Date());
        baseMapper.insert(dbInsert);
    }

    public void deleteDataByCustId(String custId) {
        UserMessageDO dbDel = new UserMessageDO();
        dbDel.setToUser(custId);
        Wrapper<UserMessageDO> wrapper = new EntityWrapper<>(dbDel);
        baseMapper.delete(wrapper);
    }

    public void setReadByCustId(String custId) {
        UserMessageDO dbModify = new UserMessageDO();
        dbModify.setToUser(custId);
        dbModify.setIsRead(FlagType.Y.name());
        dbModify.setUpdateTime(new Date());
        baseMapper.update(dbModify, null);
    }

    public void setReadId(Integer id) {
        UserMessageDO dbModify = new UserMessageDO();
        dbModify.setId(id);
        dbModify.setIsRead(FlagType.Y.name());
        dbModify.setUpdateTime(new Date());
        baseMapper.updateById(dbModify);
    }
}
