package org.iartisan.thought.server.service.bo;

/**
 * <p>
 * BO
 *
 * @author King
 * @since 2018/1/12
 */
public class CustTopicCollectBO {

    private String customerId;

    private String topicId;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }
}
