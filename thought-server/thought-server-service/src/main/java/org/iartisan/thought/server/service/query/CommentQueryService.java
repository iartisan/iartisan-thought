package org.iartisan.thought.server.service.query;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.SqlHelper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import org.iartisan.runtime.bean.Page;
import org.iartisan.runtime.bean.PageWrapper;
import org.iartisan.runtime.constants.FlagType;
import org.iartisan.runtime.support.BaseQueryServiceSupport;
import org.iartisan.runtime.utils.BeanUtil;
import org.iartisan.thought.server.dbm.mapper.CommentMapper;
import org.iartisan.thought.server.dbm.model.CommentDO;
import org.iartisan.thought.server.service.bo.CommentBO;
import org.iartisan.runtime.constants.DataStatus;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * cust query service
 *
 * @author King
 * @since 2017/12/15
 */
@Service
public class CommentQueryService extends BaseQueryServiceSupport<CommentMapper, CommentBO> {
    /**
     * 查询有效的数据
     *
     * @param page
     * @param t
     * @return
     */
    public PageWrapper<CommentBO> getEffectivePageData(Page page, CommentBO t) {
        com.baomidou.mybatisplus.plugins.Page<CommentDO> dbPage = new com.baomidou.mybatisplus.plugins.Page();
        dbPage.setCurrent(page.getCurrPage());
        dbPage.setSize(page.getPageSize());
        CommentDO dbQuery = new CommentDO();
        dbQuery.setStatus(DataStatus.E.toString());
        dbQuery.setTopicId(t.getTopicId());
        Wrapper<CommentDO> wrapper = new EntityWrapper(dbQuery);
        //按照是否采纳排序
        wrapper.orderBy("IS_ACCEPT", false);
        wrapper.orderBy("CREATE_TIME", true);
        SqlHelper.fillWrapper(dbPage, wrapper);
        dbPage.setRecords(baseMapper.selectPage(dbPage, wrapper));
        List<CommentBO> dataList = BeanUtil.copyList(dbPage.getRecords(), CommentBO.class);
        Page resPage = new Page();
        resPage.setCurrPage(dbPage.getCurrent());
        resPage.setPageSize(dbPage.getSize());
        resPage.setTotalRecords(dbPage.getTotal());
        resPage.setTotalPage(dbPage.getPages());
        PageWrapper<CommentBO> paginationBean = new PageWrapper(resPage);
        paginationBean.setData(dataList);
        return paginationBean;
    }

    public String canAccept(String topicId) {
        CommentDO commentDO = new CommentDO();
        commentDO.setTopicId(topicId);
        commentDO.setIsAccept(FlagType.Y.toString());
        Wrapper<CommentDO> dbQuery = new EntityWrapper<>(commentDO);
        int count = baseMapper.selectCount(dbQuery);
        if (count > 0) {
            return FlagType.N.toString();
        }
        return FlagType.Y.toString();
    }
}
