package org.iartisan.thought.server.service.management;

import org.iartisan.runtime.exception.NotAllowedException;
import org.iartisan.runtime.support.BaseManagementServiceSupport;
import org.iartisan.thought.server.dbm.mapper.CommentZanRelationMapper;
import org.iartisan.thought.server.dbm.model.CommentZanRelationDO;
import org.iartisan.thought.server.service.bo.CommentZanRelationBO;
import org.iartisan.thought.server.service.query.CommentZanRelationQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 *
 * @author King
 * @since 2018/2/7
 */
@Service
public class CommentZanRelationManagementService extends BaseManagementServiceSupport<CommentZanRelationMapper, CommentZanRelationBO> {


    @Autowired
    private CommentZanRelationQueryService commentZanRelationQueryService;

    public void addZanRelation(String commentId, String custId) throws NotAllowedException {
        //判断是否赞过
        boolean flag = commentZanRelationQueryService.isZan(commentId, custId);
        if (flag) {
            throw new NotAllowedException("已赞过");
        }
        CommentZanRelationDO dbInsert = new CommentZanRelationDO();
        dbInsert.setCommentId(commentId);
        dbInsert.setCustomerId(custId);
        dbInsert.setCreateTime(new Date());
        baseMapper.insert(dbInsert);
    }
}
