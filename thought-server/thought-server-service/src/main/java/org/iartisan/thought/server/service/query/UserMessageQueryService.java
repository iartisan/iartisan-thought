package org.iartisan.thought.server.service.query;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import org.iartisan.runtime.constants.FlagType;
import org.iartisan.runtime.support.BaseQueryServiceSupport;
import org.iartisan.thought.server.dbm.mapper.UserMessageMapper;
import org.iartisan.thought.server.dbm.model.UserMessageDO;
import org.iartisan.thought.server.service.bo.UserMessageBO;
import org.springframework.stereotype.Service;

/**
 * @author King
 * @since 2018/2/6
 */
@Service
public class UserMessageQueryService extends BaseQueryServiceSupport<UserMessageMapper, UserMessageBO> {

    public int getUnreadMsgCount(String to) {
        UserMessageDO userMessageDO = new UserMessageDO();
        userMessageDO.setToUser(to);
        userMessageDO.setIsRead(FlagType.N.name());
        Wrapper<UserMessageDO> wrapper = new EntityWrapper<>(userMessageDO);
        Integer count = baseMapper.selectCount(wrapper);
        return count.intValue();
    }

}
