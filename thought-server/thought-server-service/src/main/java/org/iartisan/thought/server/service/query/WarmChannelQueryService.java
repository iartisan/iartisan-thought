package org.iartisan.thought.server.service.query;


import org.iartisan.runtime.constants.FlagType;
import org.iartisan.runtime.support.BaseQueryServiceSupport;
import org.iartisan.thought.server.dbm.mapper.WarmChannelMapper;
import org.iartisan.thought.server.dbm.model.WarmChannelDO;
import org.iartisan.thought.server.service.bo.WarmChannelBO;
import org.springframework.stereotype.Service;

/**
 * @author King
 * @since 2018/1/30
 */
@Service
public class WarmChannelQueryService extends BaseQueryServiceSupport<WarmChannelMapper, WarmChannelBO> {


    public String isWarmChannelByTopicId(String topicId) {
        if (null != getWarmChannelByTopicId(topicId)) {
            return FlagType.Y.name();
        }
        return FlagType.N.name();
    }

    public WarmChannelDO getWarmChannelByTopicId(String topicId) {
        WarmChannelDO dbQuery = new WarmChannelDO();
        dbQuery.setChannelLocation(topicId);
        WarmChannelDO dbResult = baseMapper.selectOne(dbQuery);
        return dbResult;
    }

}
