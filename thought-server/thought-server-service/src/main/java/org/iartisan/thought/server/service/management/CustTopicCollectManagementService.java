package org.iartisan.thought.server.service.management;

import org.iartisan.runtime.constants.DataStatus;
import org.iartisan.runtime.support.BaseManagementServiceSupport;
import org.iartisan.thought.server.dbm.mapper.CustTopicCollectMapper;
import org.iartisan.thought.server.dbm.model.CustTopicCollectDO;
import org.iartisan.thought.server.service.bo.CustTopicCollectBO;
import org.iartisan.thought.server.service.query.CustTopicCollectQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 * collect manage
 *
 * @author King
 * @since 2018/1/12
 */
@Service
public class CustTopicCollectManagementService extends BaseManagementServiceSupport<CustTopicCollectMapper, CustTopicCollectBO> {

    @Autowired
    private CustTopicCollectQueryService custTopicCollectQueryService;

    public void collect(CustTopicCollectBO custTopicCollectBO) {
        //判断是否收藏过这个贴子
        CustTopicCollectDO collect = custTopicCollectQueryService.queryCollect(custTopicCollectBO.getCustomerId(), custTopicCollectBO.getTopicId());
        //如果收藏则删除否则添加
        if (collect != null) {
            //删除贴
            baseMapper.deleteById(collect.getId());
        } else {
            CustTopicCollectDO dbInsert = new CustTopicCollectDO();
            dbInsert.setCustomerId(custTopicCollectBO.getCustomerId());
            dbInsert.setTopicId(custTopicCollectBO.getTopicId());
            dbInsert.setCreateTime(new Date());
            dbInsert.setStatus(DataStatus.E.toString());
            baseMapper.insert(dbInsert);
        }
    }


}
