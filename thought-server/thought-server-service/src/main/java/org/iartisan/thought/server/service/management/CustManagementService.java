package org.iartisan.thought.server.service.management;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;

import org.iartisan.runtime.constants.DataStatus;
import org.iartisan.runtime.utils.UUIDUtil;
import org.iartisan.thought.server.dbm.mapper.CustBaseMapper;
import org.iartisan.thought.server.dbm.mapper.CustCipMapper;
import org.iartisan.thought.server.dbm.model.CustBaseDO;
import org.iartisan.thought.server.dbm.model.CustCipDO;
import org.iartisan.thought.server.service.constants.CustType;
import org.iartisan.thought.server.service.query.CustQueryService;
import org.iartisan.thought.server.service.support.MessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * <p>
 * cust query service
 *
 * @author King
 * @since 2017/12/15
 */
@Service
public class CustManagementService {

    @Autowired
    private CustBaseMapper custBaseMapper;

    @Autowired
    private CustCipMapper custCipMapper;

    @Transactional
    public String addCustCipData(String email, String userName, String pwd) throws IllegalArgumentException {
        //判断邮箱是否已经被注册
        CustBaseDO entity = new CustBaseDO();
        entity.setCustAccount(email);
        Wrapper<CustBaseDO> dbQuery = new EntityWrapper<>(entity);
        int count = custBaseMapper.selectCount(dbQuery);
        if (count > 0) {
            throw new IllegalArgumentException("邮箱已被注册");
        }
        String custId = UUIDUtil.shortId();
        CustBaseDO dbBaseInsert = new CustBaseDO();
        dbBaseInsert.setCustId(custId);
        dbBaseInsert.setCustAccount(email);
        dbBaseInsert.setCustType(CustType.cip.toString());
        dbBaseInsert.setCustPwd(pwd);
        //默认是待激活状态
        dbBaseInsert.setCustStatus(DataStatus.M.toString());
        dbBaseInsert.setCreateTime(new Date());
        dbBaseInsert.setCustUsername(userName);
        //添加到基表
        custBaseMapper.insert(dbBaseInsert);
        //添加到cip表
        CustCipDO dbCipInsert = new CustCipDO();
        dbCipInsert.setCustId(custId);
        dbCipInsert.setCreateTime(new Date());
        dbCipInsert.setCipEmail(email);
        custCipMapper.insert(dbCipInsert);
        return custId;
    }

    public void enableCust(String custId) {
        CustBaseDO dbModify = new CustBaseDO();
        dbModify.setCustId(custId);
        dbModify.setCustStatus(DataStatus.E.name());
        custBaseMapper.updateById(dbModify);
    }

    public void modifyAvatar(String custId, String avatar) {
        CustBaseDO dbModify = new CustBaseDO();
        dbModify.setCustId(custId);
        dbModify.setAvatar(avatar);
        custBaseMapper.updateById(dbModify);
    }

    @Transactional
    public void modifyData(String custId, String custNickName, String custGender, String sign) {
        CustBaseDO dbResult = custBaseMapper.selectById(custId);
        if (null == dbResult) {
            throw new IllegalArgumentException("没有查询到用户");
        }
        dbResult.setCustNickname(custNickName);
        dbResult.setCustGender(custGender);
        CustCipDO dbCipResult = custCipMapper.selectById(custId);
        if (null == dbCipResult) {
            dbCipResult = new CustCipDO();
            dbCipResult.setCustId(dbResult.getCustId());
            dbCipResult.setSign(sign);
            dbCipResult.setCreateTime(new Date());
            dbCipResult.setCipEmail(dbResult.getCustAccount());
            custCipMapper.insert(dbCipResult);
        } else {
            dbCipResult.setSign(sign);
            dbCipResult.setUpdateTime(new Date());
            custCipMapper.updateById(dbCipResult);
        }
        custBaseMapper.updateById(dbResult);
    }

    public void resetPwd(String custId, String oldPwd, String newPwd) {
        CustBaseDO dbResult = custBaseMapper.selectById(custId);
        if (null == dbResult) {
            throw new IllegalArgumentException("没有查询到用户");
        }
        if (!dbResult.getCustPwd().equals(oldPwd)) {
            throw new IllegalArgumentException("原密码错误");
        }
        dbResult.setCustPwd(newPwd);
        dbResult.setUpdateTime(new Date());
        custBaseMapper.updateById(dbResult);
    }

}
