package org.iartisan.thought.server.service.query;

import org.iartisan.thought.server.dbm.mapper.CustBaseMapper;
import org.iartisan.thought.server.dbm.mapper.CustCipMapper;
import org.iartisan.thought.server.dbm.model.CustBaseDO;
import org.iartisan.thought.server.dbm.model.CustCipDO;
import org.iartisan.thought.server.service.bo.CustBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * cust query service
 *
 * @author King
 * @since 2017/12/15
 */
@Service
public class CustQueryService {

    @Autowired
    private CustBaseMapper custBaseMapper;

    @Autowired
    private CustCipMapper custCipMapper;

    public CustBO getCustCipInfo(String userName, String pwd) {
        CustBaseDO dbQuery = new CustBaseDO();
        dbQuery.setCustAccount(userName);
        dbQuery.setCustPwd(pwd);
        CustBaseDO dbResult = custBaseMapper.selectOne(dbQuery);
        if (null != dbResult) {
            CustBO custBO = new CustBO();
            custBO.setCustType(dbResult.getCustType());
            custBO.setCustId(dbResult.getCustId());
            custBO.setAvatar(dbResult.getAvatar());
            custBO.setCustUserName(dbResult.getCustUsername());
            custBO.setStatus(dbResult.getCustStatus());
            return custBO;
        }
        return null;
    }

    public CustBO getCustInfoById(String custId) {
        CustBaseDO dbResult = custBaseMapper.selectById(custId);
        CustBO custBO = new CustBO();
        custBO.setCustId(dbResult.getCustId());
        custBO.setCustNickName(dbResult.getCustNickname());
        custBO.setAvatar(dbResult.getAvatar());
        custBO.setCustAccount(dbResult.getCustAccount());
        custBO.setCustGender(dbResult.getCustGender());
        custBO.setCustUserName(dbResult.getCustUsername());
        return custBO;
    }

    /**
     * 查询签名
     *
     * @param custId
     * @return
     */
    public String getSign(String custId) {
        CustCipDO dbResult = custCipMapper.selectById(custId);
        if (null != dbResult) {
            return dbResult.getSign();
        }
        return "";
    }

    public CustBaseDO getCustIdByUserName(String userName) {
        CustBaseDO dbQuery = new CustBaseDO();
        dbQuery.setCustUsername(userName);
        CustBaseDO dbResult = custBaseMapper.selectOne(dbQuery);
        if (null != dbResult) {
            return dbResult;
        }
        throw new IllegalArgumentException("查不到用户名下" + userName + "用户信息");
    }

}
