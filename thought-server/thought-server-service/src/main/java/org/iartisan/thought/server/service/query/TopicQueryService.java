package org.iartisan.thought.server.service.query;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import org.iartisan.runtime.bean.Page;
import org.iartisan.runtime.bean.PageWrapper;
import org.iartisan.runtime.support.BaseQueryServiceSupport;
import org.iartisan.runtime.utils.BeanUtil;
import org.iartisan.thought.server.dbm.mapper.TopicMapper;
import org.iartisan.thought.server.dbm.model.TopicDO;
import org.iartisan.thought.server.dbm.model.extend.TopicUserDO;
import org.iartisan.thought.server.service.bo.TopicBO;
import org.iartisan.thought.server.service.bo.extend.TopicUserBO;
import org.iartisan.runtime.constants.DataStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * interface
 *
 * @author King
 * @since 2017/12/8
 */
@Service
public class TopicQueryService extends BaseQueryServiceSupport<TopicMapper, TopicBO> {

    public List<TopicUserBO> getTopicUserList(TopicBO topicBO) {
        List<TopicUserDO> dbResult = baseMapper.selectTopicUserData(BeanUtil.copyBean(topicBO, TopicDO.class));
        return BeanUtil.copyList(dbResult, TopicUserBO.class);
    }


    public TopicUserBO getTopicUserById(String id) {
        TopicDO topicDO = new TopicDO();
        topicDO.setId(id);
        List<TopicUserDO> dbResult = baseMapper.selectTopicUserData(topicDO);
        return BeanUtil.copyBean(dbResult.get(0), TopicUserBO.class);
    }

    public TopicDO getTopicByTopicId(String topicId) {
        return baseMapper.selectById(topicId);
    }

    public PageWrapper<TopicUserBO> getTopicUserPageData(Page page, TopicBO topicBO) {
        try {
            com.baomidou.mybatisplus.plugins.Page<TopicUserDO> dbPage = new com.baomidou.mybatisplus.plugins.Page();
            dbPage.setCurrent(page.getCurrPage());
            dbPage.setSize(page.getPageSize());
            TopicUserDO dbQuery = BeanUtil.copyBean(topicBO, TopicUserDO.class);
            //不查询删除的数据
            dbQuery.setStatus(DataStatus.E.toString());
            dbPage.setRecords(baseMapper.selectTopicUserData(dbPage, dbQuery));
            List<TopicUserBO> dataList = BeanUtil.copyList(dbPage.getRecords(), TopicUserBO.class);
            Page resPage = new Page();
            resPage.setCurrPage(dbPage.getCurrent());
            resPage.setPageSize(dbPage.getSize());
            resPage.setTotalRecords(dbPage.getTotal());
            resPage.setTotalPage(dbPage.getPages());
            PageWrapper<TopicUserBO> paginationBean = new PageWrapper(resPage);
            paginationBean.setData(dataList);
            return paginationBean;
        } catch (Exception e) {
            LOGGER.error("e", e);
        }
        return null;
    }

    public List<TopicBO> getHotTopicList() {
        Wrapper<TopicDO> wrapper = new EntityWrapper<>();
        wrapper.gt("COMMENT_COUNT", 0);
        wrapper.orderBy("COMMENT_COUNT", false).last("limit 10");
        List<TopicDO> dbResult = baseMapper.selectList(wrapper);
        List<TopicBO> dataList = null;
        if (null != dbResult) {
            dataList = new ArrayList<>();
            for (TopicDO topicDO : dbResult) {
                TopicBO topicBO = new TopicBO();
                topicBO.setTitle(topicDO.getTitle());
                topicBO.setId(topicDO.getId());
                topicBO.setCommentCount(topicDO.getCommentCount());
                dataList.add(topicBO);
            }
        }
        return dataList;
    }

}
