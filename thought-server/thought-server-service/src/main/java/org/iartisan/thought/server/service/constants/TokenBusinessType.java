package org.iartisan.thought.server.service.constants;

/**
 * <p>
 * token 业务类型枚举
 *
 * @author King
 * @since 2018/5/25
 */
public enum TokenBusinessType {
    email, login
}
