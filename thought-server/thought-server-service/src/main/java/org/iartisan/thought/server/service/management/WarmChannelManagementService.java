package org.iartisan.thought.server.service.management;

import org.iartisan.runtime.support.BaseManagementServiceSupport;
import org.iartisan.thought.server.dbm.mapper.WarmChannelMapper;
import org.iartisan.thought.server.dbm.model.TopicDO;
import org.iartisan.thought.server.dbm.model.WarmChannelDO;
import org.iartisan.thought.server.service.bo.WarmChannelBO;
import org.iartisan.thought.server.service.query.TopicQueryService;
import org.iartisan.thought.server.service.query.WarmChannelQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author King
 * @since 2018/1/30
 */
@Service
public class WarmChannelManagementService extends BaseManagementServiceSupport<WarmChannelMapper, WarmChannelBO> {

    @Autowired
    private TopicQueryService topicQueryService;

    @Autowired
    private WarmChannelQueryService warmChannelQueryService;

    public void setWarmChannel(String topicId) {
        WarmChannelDO dbResult = warmChannelQueryService.getWarmChannelByTopicId(topicId);
        if (null != dbResult) {
            baseMapper.deleteById(dbResult.getId());
        } else {
            TopicDO topicDO = topicQueryService.getTopicByTopicId(topicId);
            WarmChannelDO dbInsert = new WarmChannelDO();
            dbInsert.setChannelName(topicDO.getTitle());
            dbInsert.setChannelLocation(topicDO.getId());
            //inner outer
            dbInsert.setChannelType("inner");
            dbInsert.setCreateTime(new Date());
            baseMapper.insert(dbInsert);
        }
    }

    public void deleteWarmChannel(String topicId) {
        WarmChannelDO dbResult = warmChannelQueryService.getWarmChannelByTopicId(topicId);
        if (null != dbResult) {
            baseMapper.deleteById(dbResult.getId());
        }
    }
}
